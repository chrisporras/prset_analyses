# prset_analyses

This repository include all script used to prepare and perform analysis for the PRSet paper.
We have included some data that does not require user agreement in the data folder.

As of now, the JSON files contained hard-coded path, and the script are tailored for our server. 
For users who would like to repeat our analysis, please change the path in the JSON files and also 
update the nextflow script so that it is better suited for your server.

# Dependencies

To run this pipeline, you will need the following

1. UK Biobank phenotype data in SQLite data base
    - https://gitlab.com/choishingwan/ukb_process 
2. [GreedyRelated](https://gitlab.com/choishingwan/GreedyRelated) 
    - For removing related samples, part of UK Biobank filtering pipeline
3. QCed UK Biobank data (will use 1)
    - https://gitlab.com/choishingwan/ukb-administration/-/tree/master/scripts/QC
4. [Singularity](https://singularity-tutorial.github.io/01-installation/)
5. Download the singularity container with the following command:
`singularity pull library://choishingwan/prset_analyses/prset_analyses_container:latest`

## TODO
- Update scripts to account for change to container


