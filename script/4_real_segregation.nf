nextflow.enable.dsl=2
params.version=false
params.help=false
version='0.0.3'
timestamp='2021-04-01'
if(params.version) {
    System.out.println("")
    System.out.println("Real data segregation analysis - Version: $version ($timestamp)")
    exit 1
}
params.wind3 = 10
params.wind5 = 35
params.xregion = "chr6:25000000-34000000"

if(params.help){
    System.out.println("")
    System.out.println("Real data segregation analysis - Version: $version ($timestamp)")
    System.out.println("Usage: ")
    System.out.println("    nextflow run 4_real_segregation.nf [options]")
    System.out.println("Mandatory arguments:")
    System.out.println("    --bfile       UK biobank genotype file ")
    System.out.println("    --fam         QCed fam file ")
    System.out.println("    --snp         QCed SNP file ")
    System.out.println("    --out         Output prefix ")
    System.out.println("    --gmt         Folder containing the GMT files")
    System.out.println("    --gtf         GTF reference file")
    System.out.println("    --sql         UK biobank phenotype data base")
    System.out.println("    --lassosum    lassosum Rscript")
    System.out.println("    --json        JSON contain phenotype information")
    System.out.println("    --dropout     Drop out samples")
    System.out.println("    --cov         Covariate with 40 PCs and batch")
    System.out.println("Options:")
    System.out.println("    --wind3       Padding to 3' end ")
    System.out.println("    --wind5       Padding to 5' end")
    System.out.println("    --xregion     Region to be excluded from the analysis ")
    System.out.println("    --help        Display this help messages")
}

////////////////////////////////////////////////////////////////////
//                  Helper Functions
////////////////////////////////////////////////////////////////////
def fileExists(fn){
   if (fn.exists()){
       return fn;
   }else
       error("\n\n-----------------\nFile $fn does not exist\n\n---\n")
}


////////////////////////////////////////////////////////////////////
//                  Module inclusion
////////////////////////////////////////////////////////////////////
include {   extract_phenotype_from_sql
            process_phenotype_for_segregation
            assign_fold_to_composite_trait
            assign_fold_to_single_trait
            split_samples_for_cross_validation
            dichotomize_phenotype }   from './modules/phenotype_extraction'
include {   meta_analysis
            modify_metal
            standardize_summary_statistic
            filter_summary_statistic  }   from './modules/basic_genetic_analyses'
include {   prset_analysis
            prset_analysis as highResPRSet
            extract_significant_gmt
            prsice_analysis
            prsice_analysis as allScorePRSice
            lassosum_analysis   }   from './modules/polygenic_score'
include {   supervised_pathway_classification
            supervised_classification_with_single_prs   }   from './modules/classification'
include {   gene_shift_gtf
            filter_gmt
            combine_files
            combine_files as combine_sets  }   from './modules/misc'
////////////////////////////////////////////////////////////////////
//                  Setup Channels
////////////////////////////////////////////////////////////////////


// Trying to parse the JSON input
import groovy.json.JsonSlurper
def jsonSlurper = new JsonSlurper()
// new File object from your JSON file
def ConfigFile = new File("${params.json}")
// load the text from the JSON
String ConfigJSON = ConfigFile.text
// create a dictionary object from the JSON text
def phenoConfig = jsonSlurper.parseText(ConfigJSON)

// Set based GMT files. The map groupTulpe map combo is so that 
// I can use path("*") in the process to capture all GMT files
// in one go
gmt = Channel.fromPath("${params.gmt}/*.gmt") 

// GTF file, for PRSet
gtf = Channel.fromPath("${params.gtf}")

// UK Biobank genotype data. Will check if the file exists
genotype = Channel.fromFilePairs("${params.bfile}.{bed,bim,fam}", size:3 , flat: true){ file -> file.baseName } \
    | ifEmpty{ error "No matching plink files "} \
    | map{ a -> [ fileExists(a[1]), fileExists(a[2]), fileExists(a[3])]}

// UK Biobank SQL file. This is where we extract the phenotype from
sql = Channel.fromPath("${params.sql}")

// UK Biobank covariate file 
cov = Channel.fromPath("${params.cov}")

// QC and drop out information. Use to select samples and SNPs
dropout = Channel.fromPath("${params.dropout}")
qcFam = Channel.fromPath("${params.fam}")
snp = Channel.fromPath("${params.snp}")

// Parameter for set based analysis. Default is to remove the MHC region
// and extend each gene region 35kb to 5' and 10kb to 3'
wind3 = Channel.of("${params.wind3}")
wind5 = Channel.of("${params.wind5}")

// Define the cross validation fold
cv_fold = Channel.of(1..5) \
    | flatten
maxFold = cv_fold.max()
// Software path
lassosum = Channel.fromPath("${params.lassosum}")

// Parse the summary statistic meta data
sumstat = Channel.from(phenoConfig.collect{ content ->
    [   content.name, 
        file(content.gwas), 
        content.rsid,
        content.a1,
        content.a2,
        content.statistic,
        content.pvalue,
        content.se,
        content.ncol,
        content.sampleSize,
        content.isBeta
    ]})  \
    | filter{ a -> a[0] == "LDL" || a[0] == "Height" }

// Parse the xregions from the summary statistic meta data
xregions = Channel.from(phenoConfig.collect{ content ->
    [   content.name,
        content.xregion
    ]})

// Parse the phenotype meta data
// Here, we use the GWAS as stub, 
// we won't use it at all, but this allow us to reuse processes from provious analysis
pheno = Channel.from(phenoConfig.collect{ content ->
    [   content.name, 
        file(content.phenoFile),
        file(content.gwas)   
    ]}) \
    | filter{ a -> a[0] == "LDL" || a[0] == "Height"}
    // For testing, only do LDL and height for now
    

workflow{
    // 1. Prepare the phenotype. We use the pseudo residuals as the 
    //    phenotype for our downstream analysis as using the full 
    //    covariate matrix will be too time consuming
    phenotype_preparation()
    // 2. Modify the summary statistics. Mainly want to meta-analyzed
    //    GWAS of two disease and generate the "composite" GWAS. This is
    //    not ideal as what we are doing here violates the underlying assumption
    //    of meta analysis. As a result of that, instead of using inverse variant
    //    meta-analysis, we use the p-value analysis with sample size as weight
    sumstat_preparation()
    // 3. The main polygenic score analysis. We will perform PRSet, PRSice and lassosum 
    //    on our data. There are 3 type of analyses here:
    //    1. PRSice / lassosum 
    //       1. Run PRSice or lassosum on training data set 
    //       2. Extract PRS with "best" prediction in training data
    //       3. Use the same parameter, estimate prediction using validation data
    //    2. PRSet
    //       1. Run PRSet on training data set with 10,000 permutation to obtain competitive
    //          p-value
    //       2. Select any pathways with competitive p-value < 0.05
    //       3. Use cv.glmnet with PRS of pathway selected in 2 to perform lasso and obtain "best"
    //          performing model in training dataset
    //       4. Apply the "best" glmnet model in the validation data to obtain the R2
    //    3. PRSetHybrid
    //       1. Run PRSet on training data set with 10,000 permutation to obtain competitive
    //          p-value
    //       2. Select any pathways with competitive p-value < 0.05
    //       3. Re-run PRSet on pathway selected in 2 on training data. This time, enable p-value
    //          thresholding. For each set, the p-value threshold that provide best prediction of
    //          phenotype in training data set is selected
    //       4. Use PRS from "best" p-value threshold for each gene set as input to cv.glmnet.
    //          Find best lasso parameter using training data.
    //       5. Apply the same p-value threshold and glmnet model in validation data to obtain 
    //          performance
    
    polygenic_risk_score_analysis(
        phenotype_preparation.out,
        sumstat_preparation.out
    )
}

workflow phenotype_preparation{
    // 1. Extract Extreme Height (top 5% Height in each sex), Obesity ( BMI > 30), CAD and T2D
    //    We then residualize the phenotype against Sex, Age, Centre, Batch and 15 PCs
    pheno \
        | combine(sql) \
        | extract_phenotype_from_sql 
    continuous = extract_phenotype_from_sql.out \
        | map{ a -> [   a[0],
                        a[2]]} \
        | filter{ a -> a[0] == "LDL" || a[0] == "Height"} \
        | dichotomize_phenotype
    extract_phenotype_from_sql.out \
        | map{ a -> [   a[0],
                        a[2]]} \
        | filter{ a -> a[0] != "LDL" && a[0] != "Height"} \
        | mix(continuous) \
        | combine(cov) \
        | combine(qcFam) \
        | combine(dropout) \
        | process_phenotype_for_segregation
    // we want a combination of each phenotype
    // BMI + CAD, BMI + Height, BMI + T2D, CAD + Height, CAD + T2D, Height + T2D
    // Add the equality comparison so that we don't do Height vs Obesity and Obesity vs Height
    // can add CaseOnly if we figure out how to handle comorbid samples
    // 2. Generate the composite phenotype and assign cross-validation fold to each of them
    process_phenotype_for_segregation.out \
        | combine(process_phenotype_for_segregation.out) \
        | filter(a -> a[0] != a[2]) \
        | combine(maxFold) \
        | combine(Channel.of("All", "Distinct", "Subtype")) \
        | combine(Channel.of("CV", "Sensitivity")) \
        | filter( a -> a[0] < a[2] && a[5] != "Subtype") \
        | assign_fold_to_composite_trait
    // 3. Also generate single trait data to test whether glmnet is the reason of 
    //    PRSet's performance
    process_phenotype_for_segregation.out \
        | combine(maxFold) \
        | combine(Channel.of("CV", "Sensitivity")) \
        | assign_fold_to_single_trait
    // 4. Extract samples for each cross-validation fold. For sensititivy analysis,
    //    only do one fold
    assign_fold_to_composite_trait.out.pheno  \
        | mix(assign_fold_to_single_trait.out.pheno) \
        | combine(cv_fold) \
        | filter{ a -> (a[3] == "Sensitivity" && a[5] == 1) || (a[3] != "Sensitivity")} \
        | split_samples_for_cross_validation
    // 5. Gather sample size information for ascertainment adjustment when plotting
    information = assign_fold_to_composite_trait.out.info \
        | mix(assign_fold_to_single_trait.out.info) \
        | collect 
    combine_files(  Channel.of("phenotype.info"),
                    Channel.of("result/phenotype"),
                    information)
    emit:
        split_samples_for_cross_validation.out
}

workflow sumstat_preparation{
    // need to prepare the summary statistic. The way Judit does is to first
    // perform a meta analysis
    sumstat \
        | standardize_summary_statistic \
        | combine(snp) \
        | combine(genotype) \
        | combine(xregions, by: 0) \
        | combine(Channel.of("F")) \
        | filter_summary_statistic
    filter_summary_statistic.out.sumstat \
        | combine(filter_summary_statistic.out.sumstat) \
        | filter( a -> a[0] != a[2]) \
        | filter( a -> a[0] < a[2]) \
        | map{  a -> [  a[0]+"-"+a[2],
                        a[1],
                        a[3] ]} \
        | meta_analysis \
        | modify_metal
    composite = modify_metal.out \
        | map{  a -> [  a[0].split('-')[0], // Trait A
                        a[0].split('-')[1], // Trait B
                        a[1]]}
    single = filter_summary_statistic.out.sumstat \
        | map{  a -> [  a[0],   // Trait A
                        a[0],   // Trait A
                        a[1]]}
    gwas = composite \
        | mix(single)
    emit:
        gwas
}

workflow polygenic_risk_score_analysis{
    take: pheno
    take: sumstat
    main:
        filter_gmt(gmt.collect())
        // 1. Generate frame shifted GTF file as Null
        //    If the signals resides within the genic region, this
        //    shift will allow us to test whether the performance of 
        //    PRSet is due to the genic structure (no change in performance)
        //    or were driven by true signal (significantly decreased performance)
        //    By shifting 5mb, we lost around 7% of genes, so if the decrease is 
        //    less than 7%, then it might be problematic
        gtfNorm = Channel.of("Normal") \
            | combine(gtf)
        gene_shift_gtf(gtf, "5000000")
        gtfInput = gtfNorm \
            | mix(gene_shift_gtf.out)
        // 2. Perform prset analyses on the data
        keepBest = Channel.of("true")
        high_resolution = Channel.of("true")
        competitive = Channel.of("false")
       
        baseTarget = pheno \
            | combine(sumstat, by: [0, 1]) \
            | combine(snp) \
            | combine(gtfInput) \
            | combine(genotype) \
            /* TraitA, TraitB, Fold, Type, Sensitivity, GTF type*/
            | map{ a -> [   a[0]+"-"+a[1]+"-"+a[2]+"-"+a[3]+"-"+a[4]+"-"+a[9],
                            "PhenoAdj",
                            a[5],   // target phenotype
                            a[8],   // QCed SNPs
                            a[7],   // summary statistic
                            a[11],  // bed
                            a[12],  // bim
                            a[13],  // fam
                            a[10]   /*gtf*/ ]} 
                            
        baseTarget \
            | combine(wind5) \
            | combine(wind3) \
            | combine(Channel.of(10000)) \
            | combine(filter_gmt.out) \
            | combine(keepBest) \
            | combine(competitive) \
            | prset_analysis

        // 3. For each gene set that has competitive p-value < 0.05, 
        //    we will do high resolution scoring on them to do get 
        //    optimum signal
        
        prset_analysis.out.summary \
            | combine(filter_gmt.out) \
            | extract_significant_gmt
        baseTarget \
            | combine(wind5) \
            | combine(wind3) \
            | combine(Channel.of(0)) \
            | combine(extract_significant_gmt.out, by: 0) \
            | map{ a -> [   a[0]+"-hybrid",    // Add hybrid at the end to avoid name collidsion
                            a[1],   // phenoName
                            a[2],   // Target file
                            a[3],   // QCed SNP
                            a[4],   // GWAS
                            a[5],   // bed
                            a[6],   // bim
                            a[7],   // fam
                            a[8],   // gtf
                            a[9],   // wind5
                            a[10],  // wind3
                            a[11],  // perm
                            a[12]    ]} \
            | combine(keepBest) \
            | combine(high_resolution) \
            | highResPRSet
        highRes = highResPRSet.out.summary \
            | combine(highResPRSet.out.best, by: 0) \
            | combine(highResPRSet.out.snp, by: 0) \
            | combine(Channel.of("PRSet-hybrid"))

            /* TraitA, TraitB, Fold, Type, Analysis, GTF type*/
        prsRes = prset_analysis.out.summary \
            | combine(prset_analysis.out.best, by: 0) \
            | combine(prset_analysis.out.snp, by: 0) \
            | combine(Channel.of("PRSet")) \
            | mix(highRes) \
            | map{  a -> [  a[0].split("-")[0], // TraitA
                            a[0].split("-")[1], // TraitB
                            a[0].split("-")[2], // Fold
                            a[0].split("-")[3], // Type
                            a[0].split("-")[4], // Sensitivity
                            a[0].split("-")[5], // GTF type
                            a[4],   // Software
                            a[1],   // summary
                            a[2],   // best
                            a[3]]}  // snps

        // 4. Perform classificiation using PRS from the PRSet analyses
        pheno \
            | map{  a -> [  a[0],
                            a[1],
                            a[2].toString(),
                            a[3],
                            a[4],
                            a[5],
                            a[6]]} \
            | combine(prsRes, by: [0, 1, 2, 3, 4]) \
            | supervised_pathway_classification
        
        // 5. Perform PRSice analysis using both high resolution scoring and fast score
        //    We did fast score so that we don't get like ~7000 scores from PRSice, which 
        //    can be extremely memory intensive
        singlePRSInput = pheno \
            | combine(sumstat, by: [0, 1]) \
            | combine(snp) \
            | combine(genotype) \
            /* TraitA, TraitB, Fold, Type, Sensitivity */
            | map{ a -> [   a[0]+"-"+a[1]+"-"+a[2]+"-"+a[3]+"-"+a[4],
                            "PhenoAdj",
                            a[5],   // target phenotype
                            a[8],   // QCed SNPs
                            a[7],   // summary statistic
                            a[9],  // bed
                            a[10],  // bim
                            a[11],  /* fam */]} 
        // run PRSice
        singlePRSInput \
            | combine(Channel.of("false")) \
            | prsice_analysis
        singlePRSInput \
            | combine(Channel.of("true")) \
            | allScorePRSice
        prsiceRes = prsice_analysis.out.best \
            | combine(allScorePRSice.out.all, by: 0) \
            | combine(Channel.of("PRSice"))
        // 6. Also perform PRS analysis using lassosum. This provide a rough comparison
        //    of PRSet against the state of the art software (not going to do LDpred2 as
        //    that is way too time consuming
        // our container has lassosum installed. but that doesn't include our
        // custome script that ue
        singlePRSInput \
            | combine(lassosum) \
            | combine(Channel.of("true")) \
            | combine(Channel.of("true")) \
            | lassosum_analysis
        
        singlePRS = lassosum_analysis.out.best \
            | combine(lassosum_analysis.out.all, by: 0) \
            | combine(Channel.of("lassosum")) \
            | mix(prsiceRes) \
            | map{  a -> [  a[0].split("-")[0], // TraitA
                            a[0].split("-")[1], // TraitB
                            a[0].split("-")[2], // Fold
                            a[0].split("-")[3], // Type
                            a[0].split("-")[4], // Sensitivity
                            a[3],   // Software
                            a[1],   // summary
                            a[2]]}  // best

        pheno \
            | map{  a -> [  a[0],
                            a[1],
                            a[2].toString(),
                            a[3],
                            a[4],
                            a[5],
                            a[6]]} \
            | combine(singlePRS, by: [0, 1, 2, 3, 4]) \
            | supervised_classification_with_single_prs
        result = supervised_classification_with_single_prs.out \
            | mix(supervised_pathway_classification.out.res) \
            | collect
        combine_files("${params.out}.csv", "result", result)
        set_results = supervised_pathway_classification.out.set \
            | collect 
        combine_sets("${params.out}-sets.csv", "result", set_results)
}
