library(ggplot2)
library(data.table)
library(magrittr)
library(ggsci)
library(forcats)
library(here)

dat <- fread(here("results", "subtyping", "main-full.csv"))



# Distinct Cases ----------------------------------------------------------


p <- dat %>%
    .[Sample == "Validate"] %>%
    .[GTF.Type == "" | GTF.Type == "Normal"] %>%
    .[Analysis.Type == "CV"] %>%
    .[, Trait := paste(TraitA, TraitB, sep = "-")] %>%
    ggplot(aes(
        x = fct_reorder(Trait, R2),
        y = R2,
        fill = fct_relevel(
            Software,
            "PRSetHybrid",
            "PRSetHybridNull",
            "PRSet",
            "PRSetNull",
            "lassosum",
            "lassosumGLM",
            "PRSice"
        )
    )) +
    geom_boxplot(position = position_dodge(), outlier.shape = NA) +
    theme_classic() +
    theme(
        axis.title = element_text(face = "bold", size = 16),
        axis.title.x = element_blank(),
        axis.text = element_text(size = 14),
        legend.text = element_text(size = 14),
        legend.title = element_blank()
    ) +
    labs(y = expression(paste(
        "Phenotypic variance explained by PRS (", R ^ 2, ")"
    ))) +
    scale_fill_npg() +
    scale_x_discrete(guide = guide_axis(n.dodge = 2))

ggsave(
    here("plots", "distinct-case-segregation-plot.png"),
    plot = p,
    height = 7,
    width = 10
)

# Null shift --------------------------------------------------------------


p <- dat %>%
    .[Sample == "Validate"] %>%
    .[Sample.Type == "Distinct"] %>%
    .[Software %like% "PRSet"] %>%
    .[Analysis.Type == "CV"] %>%
    dcast(., Software + Fold + TraitA + TraitB ~ GTF.Type, value.var = "R2") %>%
    .[, Diff := (Normal - Null) / Null] %>%
    .[, Trait := paste(TraitA, TraitB, sep = "-")] %>%
    ggplot(aes(
        x = fct_reorder(Trait, -Diff),
        y = Diff * 100,
        fill = fct_relevel(Software,
                           "PRSetHybrid")
    )) +
    geom_boxplot(position = position_dodge(), outlier.shape = NA) +
    theme_classic() +
    theme(
        axis.title = element_text(face = "bold", size = 16),
        axis.title.x = element_blank(),
        axis.text = element_text(size = 14),
        legend.text = element_text(size = 14),
        legend.title = element_blank()
    ) +
    labs(y = expression(paste("Relative difference in PRS ", R ^ 2, "(%)"))) +
    scale_fill_npg() +
    geom_hline(yintercept = 7, linetype = "dotted") +
    scale_x_discrete(guide = guide_axis(n.dodge = 2))

ggsave(here("plots",
            "segregation-prset-null.png"),
       height = 7,
       width = 10)
# For publication ---------------------------------------------------------

p <- dat %>%
    .[Sample == "Validate"] %>%
    .[GTF.Type == "" | GTF.Type == "Normal"] %>%
    .[Analysis.Type == "CV"] %>%
    .[, Trait := paste(TraitA, TraitB, sep = "-")] %>%
    .[Software %in% c("PRSetHybrid", "PRSice", "lassosum")] %>%
    .[, Software := fct_recode(Software, `PRSet` = "PRSetHybrid")] %>%
    .[, Subgraph := mean(R2) < 0.033, by = "Trait"] %>%
    ggplot(aes(
        x = fct_reorder(Trait, -R2),
        y = R2,
        fill = fct_relevel(Software,
                           "PRSet",
                           "lassosum",
                           "PRSice")
    )) +
    geom_boxplot(position = position_dodge(), outlier.shape = NA) +
    theme_classic() +
    theme(
        axis.title = element_text(face = "bold", size = 16),
        axis.title.x = element_blank(),
        axis.text = element_text(size = 14),
        legend.text = element_text(size = 14),
        legend.title = element_blank(),
        strip.background = element_blank(),
        strip.text = element_blank()
    ) +
    labs(y = expression(paste(
        "Phenotypic variance explained by PRS (", R ^ 2, ")"
    ))) +
    scale_fill_npg() +
    scale_x_discrete(guide = guide_axis(n.dodge = 2)) +
    facet_wrap(~ Subgraph, nrow = 2, scales = "free")


ggsave(
    here("plots", "segregation-plot-dc.png"),
    plot = p,
    height = 10,
    width = 10
)



# Control vs Either Cases -------------------------------------------------


p <- dat %>%
    .[Sample == "Validate"] %>%
    .[Sample.Type == "All"] %>%
    .[GTF.Type != "Null"] %>%
    .[Analysis.Type == "CV"] %>%
    .[, Trait := paste(TraitA, TraitB, sep = "-")] %>%
    .[Software %in% c("PRSetHybrid", "lassosum", "PRSice")] %>%
    .[, Software := fct_recode(Software, `PRSet` = "PRSetHybrid")] %>%
    ggplot(aes(
        x = fct_reorder(Trait, -R2),
        y = R2,
        fill = fct_relevel(Software,
                           "PRSet",
                           "lassosum",
                           "PRSice")
    )) +
    geom_boxplot(position = position_dodge(), outlier.shape = NA) +
    theme_classic() +
    theme(
        axis.title = element_text(face = "bold", size = 16),
        axis.title.x = element_blank(),
        axis.text = element_text(size = 14),
        legend.text = element_text(size = 14),
        legend.title = element_blank()
    ) +
    labs(y = expression(paste(
        "Phenotypic variance explained by PRS (", R ^ 2, ")"
    ))) +
    scale_fill_npg() +
    scale_x_discrete(guide = guide_axis(n.dodge = 2))

ggsave(
    here("plots", "segregation-all-samples.png"),
    plot = p,
    height = 7,
    width = 10
)


# Sensitivity analysis for distinct cases ---------------------------------


p <- dat %>%
    .[Sample == "Validate"] %>%
    .[Sample.Type == "Distinct"] %>%
    .[Analysis.Type == "Sensitivity"] %>%
    .[GTF.Type != "Null"] %>%
    .[, Trait := paste(TraitA, TraitB, sep = "-")] %>%
    .[Software %in% c("PRSetHybrid", "lassosum", "PRSice")] %>%
    .[, Software := fct_recode(Software, `PRSet` = "PRSetHybrid")] %>%
    ggplot(aes(
        x = fct_reorder(Trait, -R2),
        y = R2,
        fill = fct_relevel(Software,
                           "PRSet",
                           "lassosum",
                           "PRSice")
    )) +
    geom_col(position = position_dodge()) +
    theme_classic() +
    theme(
        axis.title = element_text(face = "bold", size = 16),
        axis.title.x = element_blank(),
        axis.text = element_text(size = 14),
        legend.text = element_text(size = 14),
        legend.title = element_blank()
    ) +
    labs(y = expression(paste(
        "Phenotypic variance explained by PRS (", R ^ 2, ")"
    ))) +
    scale_fill_npg() +
    scale_x_discrete(guide = guide_axis(n.dodge = 2))

ggsave(
    here("plots", "distinct-case-sensitivity-plot.png"),
    plot = p,
    height = 7,
    width = 7
)



# Sensitivity analysis for case control ---------------------------------


p <- dat %>%
    .[Sample == "Validate"] %>%
    .[Sample.Type == "All"] %>%
    .[GTF.Type != "Null"] %>%
    .[, Trait := paste(TraitA, TraitB, sep = "-")] %>%
    .[Software %in% c("PRSetHybrid", "lassosum", "PRSice")] %>%
    .[, Software := fct_recode(Software, `PRSet` = "PRSetHybrid")] %>%
    ggplot(aes(
        x = fct_reorder(Trait, -R2),
        y = R2,
        fill = fct_relevel(Software,
                           "PRSet",
                           "lassosum",
                           "PRSice")
    )) +
    geom_col(position = position_dodge()) +
    theme_classic() +
    theme(
        axis.title = element_text(face = "bold", size = 16),
        axis.title.x = element_blank(),
        axis.text = element_text(size = 14),
        legend.text = element_text(size = 14),
        legend.title = element_blank()
    ) +
    labs(y = expression(paste(
        "Phenotypic variance explained by PRS (", R ^ 2, ")"
    ))) +
    scale_fill_npg() +
    scale_x_discrete(guide = guide_axis(n.dodge = 2))

ggsave(
    here("plots", "case-control-sensitivity-plot.png"),
    plot = p,
    height = 7,
    width = 7
)


# Single Trait analysis ---------------------------------------------------

shaded.background <- dat %>%
    .[Sample == "Validate"] %>%
    .[Sample.Type == "Single"] %>%
    .[Analysis.Type == "CV"] %>%
    setnames(., "TraitA", "Trait") %>%
    .[GTF.Type != "Null"] %>%
    .[Software %in% c("PRSetHybrid", "lassosum", "PRSice")] %>%
    .[, Software := fct_recode(Software, `PRSet` = "PRSetHybrid")] %>%
    .[, Subgraph := R2 < 0.02] %>%
    .[Subgraph == TRUE]

p <- dat %>%
    .[Sample == "Validate"] %>%
    .[Sample.Type == "Single"] %>%
    .[Analysis.Type == "CV"] %>%
    setnames(., "TraitA", "Trait") %>%
    .[Software %in% c("PRSetHybrid", "lassosumGLM", "PRSiceGLM")] %>%
    .[, Software := fct_recode(Software, `PRSet` = "PRSetHybrid")] %>%
    .[, Subgraph := R2 < 0.02] %>%
    ggplot(aes(
        x = fct_reorder(Trait, -R2),
        y = R2,
        fill = fct_relevel(Software,
                           "PRSet",
                           "lassosumGLM",
                           "PRSiceGLM")
    )) +
    geom_boxplot(position = position_dodge(),
                 outlier.shape = NA,
                 color = NA) +
    geom_rect(
        data = shaded.background,
        fill = "lightgrey",
        color = NA,
        xmin = -Inf,
        xmax = Inf,
        ymin = -Inf,
        ymax = Inf,
        alpha = 0.1
    ) +
    geom_boxplot(position = position_dodge(), outlier.shape = NA) +
    theme_classic() +
    theme(
        axis.title = element_text(face = "bold", size = 16),
        axis.title.x = element_blank(),
        axis.text = element_text(size = 14),
        legend.text = element_text(size = 14),
        legend.title = element_blank(),
        strip.text = element_blank(),
        strip.background = element_blank()
    ) +
    labs(y = expression(paste(
        "Phenotypic variance explained by PRS (", R ^ 2, ")"
    ))) +
    scale_fill_npg() +
    scale_x_discrete(guide = guide_axis(n.dodge = 2)) +
    facet_grid(Subgraph ~ ., scales = "free_y", space = "free")

ggsave(
    here("plots", "single-triat-prediction-plot.png"),
    plot = p,
    height = 7,
    width = 7
)


# Supplementary results ---------------------------------------------------
dat.with.null <- dat %>%
    .[Sample == "Validate"] %>%
    .[b == "Single"] %>%
    .[Analysis.Type == "CV"] %>%
    setnames(., "TraitA", "Trait") %>%
    .[Software %in% c("PRSetHybrid", "lassosumGLM", "PRSiceGLM")] %>%
    .[, Software := fct_recode(Software, `PRSet` = "PRSetHybrid")] %>%
    dcast(., Trait + Fold ~ Software + GTF.Type, value.var = "R2") %>%
    melt(., id.var = c("Trait", "Fold")) %>%
    .[, variable := gsub("_$", "", variable)] %>%
    setnames(., c("variable", "value"), c("Software", "R2"))
shaded.background <- dat.with.null %>%
    .[, Subgraph := R2 < 0.02] %>%
    .[Subgraph == TRUE]

p <- dat.with.null %>%
    .[, Subgraph := R2 < 0.02] %>%
    ggplot(aes(
        x = fct_reorder(Trait, -R2),
        y = R2,
        fill = fct_relevel(
            Software,
            "PRSet_Normal",
            "lassosumGLM",
            "PRSiceGLM",
            "PRSet_Null"
        )
    )) +
    geom_boxplot(position = position_dodge(),
                 outlier.shape = NA,
                 color = NA) +
    geom_rect(
        data = shaded.background,
        fill = "lightgrey",
        color = NA,
        xmin = -Inf,
        xmax = Inf,
        ymin = -Inf,
        ymax = Inf,
        alpha = 0.1
    ) +
    geom_boxplot(position = position_dodge(), outlier.shape = NA) +
    theme_classic() +
    theme(
        axis.title = element_text(face = "bold", size = 16),
        axis.title.x = element_blank(),
        axis.text = element_text(size = 14),
        legend.text = element_text(size = 14),
        legend.title = element_blank(),
        strip.text = element_blank(),
        strip.background = element_blank()
    ) +
    labs(y = expression(paste(
        "Phenotypic variance explained by PRS (", R ^ 2, ")"
    ))) +
    scale_fill_npg() +
    scale_x_discrete(guide = guide_axis(n.dodge = 2)) +
    facet_grid(Subgraph ~ ., scales = "free_y", space = "free")
