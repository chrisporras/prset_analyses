library(ggsci)
library(ggplot2)
library(magrittr)
library(data.table)
library(forcats)
library(here)
library(ggpubr)
publication <- F
suffix <- ifelse(publication, "tiff", "png")

files <-
    list.files(
        pattern = "celltype.csv$",
        path = here("results", "real-data"),
        full.names = T
    )


result <- NULL
for (i in files) {
    tmp <- fread(i)
    name <- strsplit(i, split = "/") %>%
        unlist %>%
        tail(n = 1) %>%
        strsplit(., split = "-") %>%
        unlist %>%
        head(n = 1)
    tmp[, Trait := name]
    result %<>%
        rbind(., tmp)
}


result[, Trait := fct_recode(
    Trait,
    Schizophrenia = "SCZ",
    `Alcohol Consumption` = "Alcohol",
    `Alzheimer's Disease` = "AD",
    `Coronary Artery Disease` = "CAD"
)] %>%
    .[, Type :=
          fct_recode(Algorithm,
                     `Non-linear assumption` = "Top Quantile",
                     `Linear assumption` = "lm signed P on quantile (one sided)")] %>%
    .[, Data.Source :=
          fct_recode(Data.Source,
                     `Brain Cell Types` = "Cell",
                     `Tissue Types (GTEx)` = "Tissue")]

plot.theme <- theme_classic() +
    theme(
        strip.background = element_blank(),
        axis.title.x = element_blank(),
        axis.text.x = element_text(angle = 45, hjust = 1),
        axis.text = element_text(size = 16),
        legend.title = element_blank(),
        axis.title = element_text(face = "bold", size = 16),
        strip.text = element_blank(),
        legend.text = element_text(size = 16),
        plot.title = element_text(size = 16, face = "bold", hjust = 0.5),
        legend.key = element_rect(color = "black")
    )



plot.specificity <- function(input, data.source) {
    p <- input[Data.Source == data.source] %>%
        ggplot(aes(
            x = Trait,
            y = -log10(P.full),
            fill = fct_relevel(Software, "PRSet", "MAGMA", "LDSC"),
            color = fct_relevel(Software, "PRSet", "MAGMA", "LDSC"),
            alpha = Type
        )) +
        geom_col(position = position_dodge(width = 0.8),
                 width = 0.7) +
        geom_hline(yintercept = -log10(0.05 / 6), linetype = "dotted") +
        plot.theme +
        scale_fill_npg() +
        scale_color_npg() +
        labs(y = bquote(atop(
            Association ~ with ~ expert, expectation ~ (-log[10] ~ italic(P))
        )))  +
        scale_y_continuous(expand = c(0, 0)) +
        facet_wrap( ~ Type) +
        ggtitle(data.source)
    return(p)
}


gtex <- plot.specificity(result, "Tissue Types (GTEx)")
brain <- plot.specificity(result, "Brain Cell Types")

ggarrange(
    gtex,
    brain,
    common.legend = T,
    nrow = 2,
    ncol = 1,
    labels = "auto",
    legend = "right"
)

ggsave(here("plots", paste0("figure4.", suffix)),
       height = 10,
       width = 10)

# Trait specific results --------------------------------------------------

scz.raw <-
    fread(here("results", "real-data", "SCZ-celltype-raw.csv"))

# Obtain GTEx encoding

gtex.info <-
    fread(here(
        "data",
        "GTEx_Analysis_v8_Annotations_SampleAttributesDS.txt"
    )) %>%
    .[, c("SMTS", "SMTSD")] %>%
    unique %>%
    .[, SMTSD := sapply(SMTSD, function(i) {
        gsub("- ", "", i) %>%
            gsub(" ", "_", .) %>%
            gsub(":", "_", .) %>%
            gsub("-", "_", .) %>%
            gsub("\\(", "", .) %>%
            gsub("\\)", "", .)
    })] %>%
    .[SMTS != "Brain", SMTS := "Others"]
scz.res <- merge(scz.raw, gtex.info, by.x = "Set", by.y = "SMTSD")
num.tissue <- unique(scz.res$Set) %>% length

tissue.specificity.plot <- function(input, algorithm) {
    p <-  input %>%
        .[Algorithm == algorithm] %>%
        ggplot(aes(
            x = fct_reorder(Set, -log10(P),
                            .fun = mean, na.rm = TRUE),
            y = -log10(P),
            fill = fct_relevel(Software, "PRSet", "MAGMA", "LDSC")
        )) +
        geom_bar(stat = "identity", position = position_dodge()) +
        geom_hline(yintercept = -log10(0.05 / num.tissue),
                   linetype = "dotted") +
        theme_classic() +
        theme(
            axis.title.y = element_blank(),
            axis.title.x = element_text(size = 16, face = "bold"),
            legend.title = element_blank(),
            legend.text = element_text(size = 14)
        ) +
        labs(y = bquote(-log[10](italic(P) ~ value))) +
        coord_flip() +
        facet_grid(SMTS ~ Software, scale = "free_y", space = "free_y") +
        guides(fill = guide_legend(title = "Software")) +
        scale_fill_npg()
    return(p)
}

brain.specificity.plot <- function(input, algorithm) {
    num.set <- unique(input$Set) %>% length
    p <-  input %>%
        .[Algorithm == algorithm] %>%
        ggplot(aes(
            x = fct_reorder(Set, -log10(P), .fun = mean),
            y = -log10(P),
            fill = fct_relevel(Software, "PRSet", "MAGMA", "LDSC")
        )) +
        geom_col(position = position_dodge()) +
        coord_flip() +
        geom_hline(yintercept = -log10(0.05 / num.set),
                   linetype = "dotted") +
        theme_classic() +
        theme(
            axis.title.y = element_blank(),
            axis.title.x = element_text(size = 16, face = "bold"),
            legend.title = element_blank(),
            legend.text = element_text(size = 14)
        ) +
        scale_fill_npg()
    return(p)
}

scz.nonlinear <- tissue.specificity.plot(scz.res, "Top Quantile")
scz.linear <-
    tissue.specificity.plot(scz.res, "lm signed P on quantile (one sided)")
ggsave(
    here("plots", paste0("scz-nonlinear.", suffix)),
    plot = scz.nonlinear,
    height = 15,
    width = 10
)
ggsave(
    here("plots", paste0("scz-linear.", suffix)),
    plot = scz.linear,
    height = 15,
    width = 10
)

scz.cell.nonlinear <- brain.specificity.plot(scz.raw[!Set %in% gtex.info$SMTSD], "Top Quantile")
scz.cell.linear <- brain.specificity.plot(scz.raw[!Set %in% gtex.info$SMTSD], "lm signed P on quantile (one sided)")
ggsave(
    here("plots", paste0("scz-nonlinear-cell.", suffix)),
    plot = scz.cell.nonlinear,
    height = 15,
    width = 10
)
ggsave(
    here("plots", paste0("scz-linear-cell.", suffix)),
    plot = scz.cell.linear,
    height = 15,
    width = 10
)

# AD celltype -------------------------------------------------------------


ad.raw <-
    fread(here("results", "real-data", "AD-celltype-raw.csv")) %>%
    .[!Set %in% gtex.info$SMTSD]
num.set <- unique(ad.raw$Set) %>% length

ad.nonlinear <- brain.specificity.plot(ad.raw, "Top Quantile")
ad.linear <- brain.specificity.plot(ad.raw, "lm signed P on quantile (one sided)")


ggsave(
    here("plots", paste0("ad-nonlinear.", suffix)),
    plot = ad.nonlinear,
    height = 15,
    width = 7
)
ggsave(
    here("plots", paste0("ad-linear.", suffix)),
    plot = ad.linear,
    height = 15,
    width = 7
)

