#!/usr/bin/env nextflow

////////////////////////////////////////////////////////////////////
//
//      This script is responsible to perform the comparison of 
//  performance between PRSet, PRSice and lassosum on quantitative
//  traits without performing subtyping
//
////////////////////////////////////////////////////////////////////

nextflow.enable.dsl=2
params.version=false
params.help=false
version='0.0.1'
timestamp='2021-05-11'
if(params.version) {
    System.out.println("")
    System.out.println("Run single trait performance comparison  - Version: $version ($timestamp)")
    exit 1
}

params.wind3 = 10
params.wind5 = 35
params.xregion = "chr6:25000000-34000000"
if(params.help){
    System.out.println("")
    System.out.println("Run single trait performance comparison - Version: $version ($timestamp)")
    System.out.println("Usage: ")
    System.out.println("    nextflow run 7_single_trait_performance.nf [options]")
	System.out.println("Mandatory arguments:")
    System.out.println("    --bfile       UK biobank genotype file ")
    System.out.println("    --fam         QCed fam file ")
    System.out.println("    --snp         QCed SNP file ")
    System.out.println("    --out         Output prefix ")
    System.out.println("    --gmt         Folder containing the GMT files")
    System.out.println("    --gtf         GTF reference file")
    System.out.println("    --sql         UK biobank phenotype data base")
    System.out.println("    --prsice      PRSice executable")
    System.out.println("    --lassosum    lassosum Rscript")
    System.out.println("    --json        JSON contain phenotype information")
    System.out.println("    --dropout     Drop out samples")
    System.out.println("    --cov         Covariate with 40 PCs and batch")
    System.out.println("Options:")
    System.out.println("    --wind3       Padding to 3' end ")
    System.out.println("    --wind5       Padding to 5' end")
    System.out.println("    --help        Display this help messages")
} 


////////////////////////////////////////////////////////////////////
//                  Helper Functions
////////////////////////////////////////////////////////////////////
def fileExists(fn){
   if (fn.exists()){
       return fn;
   }else
       error("\n\n-----------------\nFile $fn does not exist\n\n---\n")
}

def gen_file(a, bgen){
    return bgen.replaceAll("@",a.toString())
}

def get_chr(a, input){
    if(input.contains("@")){
        return a
    }
    else {
        return 0
    }
}

////////////////////////////////////////////////////////////////////
//                  Module inclusion
////////////////////////////////////////////////////////////////////
include {   modify_summary_statistic_for_segregation
    }   from './modules/basic_genetic_analyses'
include {   extract_phenotype_from_sql
            residualize_phenotypes
            assign_fold_to_phenotypes
            split_samples_for_cross_validation  }   from './modules/phenotype_extraction'
include {   gene_shift_gtf }   from './modules/misc'
include {   first_pass_prset
            high_res_second_pass_prset
            cross_trait_prsice_cv_analysis
            cross_trait_fast_prsice_cv_analysis
            cross_trait_lassosum_cv_analysis  }   from './modules/polygenic_score'
include {   prediction_with_pathway
            cross_trait_single_prs_classification
            modify_classification_results
            sipmle_result_combine }   from './modules/classification'
////////////////////////////////////////////////////////////////////
//                  Setup Channels
////////////////////////////////////////////////////////////////////


// Trying to parse the JSON input
import groovy.json.JsonSlurper
def jsonSlurper = new JsonSlurper()
// new File object from your JSON file
def ConfigFile = new File("${params.json}")
// load the text from the JSON
String ConfigJSON = ConfigFile.text
// create a dictionary object from the JSON text
def phenoConfig = jsonSlurper.parseText(ConfigJSON)

// Set based GMT files. The map groupTulpe map combo is so that 
// I can use path("*") in the process to capture all GMT files
// in one go
gmt = Channel.fromPath("${params.gmt}/*.gmt") \
    | map{ a -> [ "tmp", a]} \
    | groupTuple \
    | map{ a -> [ a[1] ]}

// GTF file, for PRSet
gtf = Channel.fromPath("${params.gtf}")

// UK Biobank genotype data. Will check if the file exists
genotype = Channel.fromFilePairs("${params.bfile}.{bed,bim,fam}", size:3 , flat: true){ file -> file.baseName } \
    | ifEmpty{ error "No matching plink files "} \
    | map{ a -> [ fileExists(a[1]), fileExists(a[2]), fileExists(a[3])]}

// UK Biobank SQL file. This is where we extract the phenotype from
sql = Channel.fromPath("${params.sql}")

// UK Biobank covariate file 
cov = Channel.fromPath("${params.cov}")

// QC and drop out information. Use to select samples and SNPs
dropout = Channel.fromPath("${params.dropout}")
qcFam = Channel.fromPath("${params.fam}")
snp = Channel.fromPath("${params.snp}")

// Parameter for set based analysis. Default is to remove the MHC region
// and extend each gene region 35kb to 5' and 10kb to 3'
wind3 = Channel.of("${params.wind3}")
wind5 = Channel.of("${params.wind5}")
xregion = Channel.of("${params.xregion}")
// Define the cross validation fold. Wish I can feed maxFold into cv_fold.
// Haven't figure out a way yet
maxFold = Channel.of(10)
cv_fold = Channel.of(1..10) \
    | flatten

// Software path
lassosum = Channel.fromPath("${params.lassosum}")
prsice = Channel.fromPath("${params.prsice}")

// Parse the summary statistic meta data
sumstat = Channel.from(phenoConfig.collect{ content ->
    [   content.name, 
        file(content.gwas), 
        content.rsid,
        content.a1,
        content.a2,
        content.statistic,
        content.pvalue,
        content.se,
        content.ncol,
        content.sampleSize,
        content.isBeta
    ]})

 
pheno = Channel.from(phenoConfig.collect{ content ->
    [   content.name, 
        file(content.phenoFile), 
        file(content.rscript)
    ]})

workflow{
    // 1. Prepare the phenotype. We use the pseudo residuals as the 
    //    phenotype for our downstream analysis as using the full 
    //    covariate matrix will be too time consuming
    phenotype_preparation()
    // 2. Modify the summary statistics to have a unified input
    sumstat_preparation()  
    // 3. Run polygenic risk score analysis
    
    polygenic_risk_score_analysis(
        phenotype_preparation.out,
        sumstat_preparation.out
    )
}


workflow sumstat_preparation{
    // prepare summary statistics so that they have identical formats
    sumstat \
        | modify_summary_statistic_for_segregation
    emit:
        modify_summary_statistic_for_segregation.out
}

workflow phenotype_preparation{
    // 1. Extract Phenotype from SQL 
    // 2. Residualize phenotype
    phenotype = pheno \
        | combine(sql) \
        | extract_phenotype_from_sql \
        | combine(qcFam) \
        | combine(dropout) \
        | combine(cov) \
        | residualize_phenotypes \
        | combine(maxFold) \
        | assign_fold_to_phenotypes \
        | combine(Channel.of("stub")) \
        | combine(Channel.of("stub")) \
        | combine(Channel.of("stub")) \
        | map{ a -> [   a[0],   // phenotype
                        a[2],   // stubs for code reuse
                        a[3],   // same as above    
                        a[4],   // same as above
                        a[1]]}  /* Phenotype file */ \
        | combine(cv_fold) \
        | split_samples_for_cross_validation \
        | map{ a -> [   a[0],   // phenotype
                        a[2],   // fold
                        a[5],   // training
                        a[6]]}  // validate
    emit:
       phenotype
}


workflow polygenic_risk_score_analysis{
    take: pheno
    take: sumstat
    main:
        // 1. Generate frame shifted GTF file as Null
        //    If the signals resides within the genic region, this
        //    shift will allow us to test whether the performance of 
        //    PRSet is due to the genic structure (no change in performance)
        //    or were driven by true signal (significantly decreased performance)
        //    By shifting 5mb, we lost around 7% of genes, so if the decrease is 
        //    less than 7%, then it might be problematic
        gtfNorm = gtf \
            | combine(Channel.of("Normal"))
        gtfNull = gene_shift_gtf(gtf, "5000000")
        gtfInput = gtfNorm \
            | mix(gtfNull) \
            | map{ a -> [   a[1],
                            a[0]]}
        // 2. Perform prset analyses on the data
        // 3. On each gene set that has competitive p-value < 0.05, 
        //    we will do high resolution scoring on them to do get 
        //    optimum signal
        pheno \
            | combine(sumstat, by: 0) \
            | combine(genotype) \
            | combine(gtfInput) \
            | combine(snp) \
            | combine(wind3) \
            | combine(wind5) \
            | combine(xregion) \
            | combine(prsice) \
            | combine(gmt) \
            | first_pass_prset \
            | combine(gtfInput, by: 0) \
            | combine(genotype) \
            | combine(snp) \
            | combine(wind3) \
            | combine(wind5) \
            | combine(xregion) \
            | combine(prsice) \
            | combine(gmt) \
            | high_res_second_pass_prset
        // 4. Perform classificiation using PRS from the PRSet analyses
        high_res_second_pass_prset.out \
            | prediction_with_pathway
        // 5. Perform PRSice analysis using both high resolution scoring and fast score
        //    We did fast score so that we don't get like ~7000 scores from PRSice, which 
        //    can be extremely memory intensive


        pheno \
            | combine(sumstat, by: 0) \
            | combine(Channel.of("TraitB")) \
            | combine(Channel.of("Type")) \
            | combine(Channel.of("Analysis")) \
            | map{ a -> [   a[0],   // Phenotype Name
                            a[5],   // stub
                            a[1],   // fold
                            a[6],   // type
                            a[7],   // analysis
                            a[2],   // training
                            a[3],   // validate
                            a[4]    /* sumstat */]} \
            | combine(genotype) \
            | combine(snp) \
            | combine(xregion) \
            | combine(prsice) \
            | (cross_trait_prsice_cv_analysis & cross_trait_fast_prsice_cv_analysis) 
        prsiceRes = cross_trait_prsice_cv_analysis.out \
            | combine(cross_trait_fast_prsice_cv_analysis.out, by: [0, 1, 2, 3, 4, 5])
        // 6. Also perform PRS analysis using lassosum. This provide a rough comparison
        //    of PRSet against the state of the art software (not going to do LDpred2 as
        //    that is way too time consuming
        pheno \
            | combine(sumstat, by: 0) \
            | combine(Channel.of("TraitB")) \
            | combine(Channel.of("Type")) \
            | combine(Channel.of("Analysis")) \
            | map{ a -> [   a[0],   // Phenotype Name
                            a[5],   // stub
                            a[1],   // fold
                            a[6],   // type
                            a[7],   // analysis
                            a[2],   // training
                            a[3],   // validate
                            a[4]    /* sumstat */]} \
            | combine(genotype) \
            | combine(snp) \
            | combine(xregion) \
            | combine(lassosum) \
            | cross_trait_lassosum_cv_analysis
        
        cross_trait_lassosum_cv_analysis.out \
            | mix(prsiceRes) \
            | cross_trait_single_prs_classification \
            | modify_classification_results
        results = prediction_with_pathway.out.res \
            | mix(modify_classification_results.out) \
            | collect 
        sipmle_result_combine("main", results)
}
