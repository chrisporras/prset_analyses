// Try to perform unsupervised subtyping using IBD, which have known
// subtypes that we can use for validation
nextflow.enable.dsl=2
params.version=false
params.help=false
version='0.0.1'
timestamp='2021-05-18'
if(params.version) {
    System.out.println("")
    System.out.println("Unsupervised subtyping - Version: $version ($timestamp)")
    exit 1
}
params.wind3 = 10
params.wind5 = 35
params.perm = 10000


if(params.help){
    System.out.println("")
    System.out.println("Unsupervised subtyping - Version: $version ($timestamp)")
    System.out.println("Usage: ")
    System.out.println("    nextflow run 8_unsupervised_subtyping.nf [options]")
	System.out.println("Mandatory arguments:")
    System.out.println("    --bfile       UK biobank genotype file ")
    System.out.println("    --fam         QCed fam file ")
    System.out.println("    --snp         QCed SNP file ")
    System.out.println("    --out         Output prefix ")
    System.out.println("    --gmt         Folder containing the GMT files")
    System.out.println("    --gtf         GTF reference file")
    System.out.println("    --sql         UK biobank phenotype data base")
    System.out.println("    --prsice      PRSice executable")
    System.out.println("    --json        JSON contain phenotype information")
    System.out.println("    --dropout     Drop out samples")
    System.out.println("    --cov         Covariate with 40 PCs and batch")
    System.out.println("Options:")
    System.out.println("    --wind3       Padding to 3' end ")
    System.out.println("    --wind5       Padding to 5' end")
    System.out.println("    --help        Display this help messages")
}


////////////////////////////////////////////////////////////////////
//                  Helper Functions
////////////////////////////////////////////////////////////////////
def fileExists(fn){
   if (fn.exists()){
       return fn;
   }else
       error("\n\n-----------------\nFile $fn does not exist\n\n---\n")
}

def gen_file(a, bgen){
    return bgen.replaceAll("@",a.toString())
}

def get_chr(a, input){
    if(input.contains("@")){
        return a
    }
    else {
        return 0
    }
}

////////////////////////////////////////////////////////////////////
//                  Module inclusion
////////////////////////////////////////////////////////////////////
include {   filter_gmt }   from './modules/misc'
include {   extract_phenotype_from_sql
            generate_ibd_folds
            extract_ibd_folds  }   from './modules/phenotype_extraction'
include {   modify_summary_statistic   }   from './modules/basic_genetic_analyses'
include {   ibd_prset_analysis
            ibd_high_res_analysis
            modify_prset_result
            ibd_prsice_analysis    }   from './modules/polygenic_score'
include {   ibd_subtype_classification
            combine_ibd_result
            ibd_single_classification  }   from './modules/classification.nf'
////////////////////////////////////////////////////////////////////
//                  Setup Channels
////////////////////////////////////////////////////////////////////


// Trying to parse the JSON input
import groovy.json.JsonSlurper
def jsonSlurper = new JsonSlurper()
// new File object from your JSON file
def ConfigFile = new File("${params.json}")
// load the text from the JSON
String ConfigJSON = ConfigFile.text
// create a dictionary object from the JSON text
def phenoConfig = jsonSlurper.parseText(ConfigJSON)
// Parse the summary statistic meta data
sumstat = Channel.from(phenoConfig.collect{ content ->
    [   content.name, 
        file(content.gwas), 
        content.rsid,
        content.a1,
        content.a2,
        content.statistic,
        content.pvalue,
        content.se,
        content.ncol,
        content.sampleSize,
        content.isBeta,
        content.xregion
    ]})

// Parse the phenotype meta data
pheno = Channel.from(phenoConfig.collect{ content ->
    [   content.name, 
        file(content.phenoFile),
        file(content.rscript)   
    ]}) \
    | filter{ a -> a[0] == "IBD"}

biochemGMT = Channel.fromPath("${params.gmt}/*.gmt") 
gtf = Channel.fromPath("${params.gtf}")
genotype = Channel.fromFilePairs("${params.bfile}.{bed,bim,fam}", size:3 , flat: true){ file -> file.baseName } \
    | ifEmpty{ error "No matching plink files "} \
    | map{ a -> [ fileExists(a[1]), fileExists(a[2]), fileExists(a[3])]}
sql = Channel.fromPath("${params.sql}")
dropout = Channel.fromPath("${params.dropout}")
qcFam = Channel.fromPath("${params.fam}")
snp = Channel.fromPath("${params.snp}")
wind3 = Channel.of("${params.wind3}")
wind5 = Channel.of("${params.wind5}")
cov = Channel.fromPath("${params.cov}")
prsice=Channel.fromPath("${params.prsice}")
fold = Channel.of(1..5)
workflow{
    filter_gmt(biochemGMT.collect())
    phenotype_preprocessing()
    sumstat_preprocessing()
    polygenic_analysis(
        phenotype_preprocessing.out,
        sumstat_preprocessing.out,
        filter_gmt.out
    )
}

workflow phenotype_preprocessing{
    // Generate the required data 
    pheno \
        | combine(sql) \
        | extract_phenotype_from_sql \
        | combine(qcFam) \
        | combine(dropout) \
        | combine(cov) \
        | combine(fold.max()) \
        | generate_ibd_folds \
        | combine(fold) \
        | combine(Channel.of("Supervised", "Unsupervised"))
        | extract_ibd_folds
    emit:
        extract_ibd_folds.out
}

workflow sumstat_preprocessing{
    sumstat \
            | combine(snp) \
            | combine(genotype) \
            | modify_summary_statistic
    emit:
        modify_summary_statistic.out
}

workflow polygenic_analysis{
    take: samples
    take: gwas
    take: gmt
    main:
        // 1. Run PRSet with 10,000 set based permutation
        samples \
            | combine(gwas) \
            | combine(genotype) \
            | combine(gtf) \
            | combine(wind3) \
            | combine(wind5) \
            | combine(prsice) \
            | combine(gmt) \
            | (ibd_prset_analysis & ibd_prsice_analysis)
        ibd_prset_analysis.out \
            | ibd_high_res_analysis \
            | ibd_subtype_classification
        ibd_prsice_analysis.out  \
            | ibd_single_classification 
        ibd_single_classification.out \
            | mix(ibd_subtype_classification.out) \
            | collect \
            | combine_ibd_result
}