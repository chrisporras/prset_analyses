#!/usr/bin/env nextflow
////////////////////////////////////////////////////////////////////
//
//      This script is responsible to download (where possible),
//  process and modify data required for downstream real 
//  data analyses. It will also generate the required 
//  LD Scores. Users should run this script first before running
//  any other scripts.
//
//      Note: MalaCards Score and MSigDB GMT files need to be manually 
//  downloaded. For MSigDB, use the gene name GMT files instead of 
//  the entrez files
//
////////////////////////////////////////////////////////////////////

nextflow.enable.dsl=2
params.version=false
params.help=false
version='0.0.4'
timestamp='2021-03-04'
if(params.version) {
    System.out.println("")
    System.out.println("Prepare Workspace - Version: $version ($timestamp)")
    exit 1
}
params.min = 10
params.max = 2000
params.wind3 = 10
params.wind5 = 35
params.xregion = "chr6:25000000-34000000"
if(params.help){
    System.out.println("")
    System.out.println("Prepare Workspace - Version: $version ($timestamp)")
    System.out.println("Usage: ")
    System.out.println("    nextflow run prepare_workspace.nf [options]")
    System.out.println("Mandatory arguments:")
    System.out.println("    --malacard    Manually curated MalaCard info")
    System.out.println("    --fam         QCed fam file ")
    System.out.println("    --snp         QCed SNP file ")
    System.out.println("    --out         Output prefix ")
    System.out.println("    --ref         Reference genotype for LD score calculation")
    System.out.println("    --pop         Population information for reference data")
    System.out.println("    --printsnp    Print SNP file from base line")
    System.out.println("    --msigdb      Folder containing the downloaded MSigDB files")
    System.out.println("Options:")
    System.out.println("    --min         Minimum number of genes required in a gene set")
    System.out.println("    --max         Maximum number of genes allowed in a gene set")
    System.out.println("    --wind3       Padding to 3' end ")
    System.out.println("    --wind5       Padding to 5' end")
    System.out.println("    --quantiles   Number of quantiles to use")
    System.out.println("    --xregion     Region to be excluded from the analysis ")
    System.out.println("    --thread      Number of thread use")
    System.out.println("    --help        Display this help messages")
}
////////////////////////////////////////////////////////////////////
//                  Helper Functions
////////////////////////////////////////////////////////////////////
def fileExists(fn){
   if (fn.exists()){
       return fn;
   }else
       error("\n\n-----------------\nFile $fn does not exist\n\n---\n")
}

def get_prefix(str){
    if(str.split('-')[0] == "NA"){
        error("\n\n----------\n$str cannot be tokenized\n")
    }else 
        return str.split('-')[0];
}
def gen_file(chr, input){
    return input.replaceAll("#",chr.toString())
}

def get_chr(a, input){
    if(input.contains("#")){
        return a
    }
    else {
        return 0
    }
}

////////////////////////////////////////////////////////////////////
//                  Module inclusion
////////////////////////////////////////////////////////////////////
include {   generate_ensembl_map
            generate_go_sets
            generate_mgi_sets
            filter_msigdb_sets
            combine_pathway_information    } from './modules/generate_biochemical_sets'
include {   generate_gtex_sets
            generate_celltype_sets  } from './modules/generate_specificity_sets'
include {   genotype_filtering
            annotate_genome
            trim_annotation
            annotate_baseline_genome
            calculate_ldscore
            unwrap_ldscore
            zip_ldsc
            obtain_baseline_bed
            update_baseline
            rename_baseline_files  }   from './modules/calculate_ldscore'

////////////////////////////////////////////////////////////////////
//                  Download required data
////////////////////////////////////////////////////////////////////
gtf=file("ftp://ftp.ensembl.org/pub/release-75/gtf/homo_sapiens/Homo_sapiens.GRCh37.75.gtf.gz")
gene_ontology=file("http://geneontology.org/gene-associations/goa_human.gaf.gz")
obo=file("http://purl.obolibrary.org/obo/go.obo")
mgi_rpt=file("http://www.informatics.jax.org/downloads/reports/HMD_HumanPhenotype.rpt")
mgi_obo=file("http://www.informatics.jax.org/downloads/reports/MPheno_OBO.ontology")
mgi_map=file("http://www.informatics.jax.org/downloads/reports/MGI_PhenoGenoMP.rpt")
////////////////////////////////////////////////////////////////////
//                  Setup Channels
////////////////////////////////////////////////////////////////////
msigdb=Channel.fromPath("${params.msigdb}/*")
// For printsnp, try to use the QCed SNP list from UK Biobank so that we maximize overlap in downstream analyses
printsnp = Channel.fromPath("${params.printsnp}")
malacard=Channel.fromPath("${params.malacard}")
chromosome = Channel.value(1..22).flatten()
reference = chromosome \
    | map{ a -> [   get_chr(a, "${params.ref}"), 
                    fileExists(file(gen_file(a, "${params.ref}.bed"))),
                    fileExists(file(gen_file(a, "${params.ref}.bim"))),
                    fileExists(file(gen_file(a, "${params.ref}.fam")))]}
pop = Channel.fromPath("${params.pop}")
minSize = Channel.of("${params.min}")
maxSize = Channel.of("${params.max}")
quantiles = Channel.of("${params.quantiles}")

// TODO: Auto record of access date
workflow{
    // 1. Generate and filter the biochemical sets
    generate_biochemical_sets()
    // 2. Calculate Tissue specificity and generate 
    //    gene sets required for the specificity analysis
    generate_specificity_sets(
        generate_biochemical_sets.out.ensembl,
        generate_biochemical_sets.out.ortholog
    )
    
    biochemical = generate_biochemical_sets.out.gmt \
        | combine(Channel.of("biochem"))
    specificity = generate_specificity_sets.out.gmt \
        | combine(Channel.of("specificity"))
    gmt = biochemical \
        | mix(specificity)
    
    // 3. As we are comparing PRSet with MAGMA and LDSC, we will need
    //    to calculate set based LD scores. As we are processing more 
    //    sets than those provided by the authors of LDSC, we will 
    //    need to calculate each scores ourselve, which is a very time
    //    consuming process
    //    It is also exceptionally challenging given the amount of
    //    file generated (22 * 8156 * 4 = 717728)
    generate_ldscores(
        generate_biochemical_sets.out.gtf, 
        gmt
    )
}

workflow generate_biochemical_sets{
    main:
        // 1. Generate the gene name to ensembl ID translation map
        generate_ensembl_map(gtf)
        // 2. process the GO data and generate the required GMT files.
        //    All obsolete GO will be removed, as with any GO terms 
        //    with less than minSize and more than maxSize genes 
        generate_go_sets(
            generate_ensembl_map.out.ensembl, 
            gene_ontology, 
            obo, 
            malacard,
            minSize,
            maxSize)
        // 3. process the MGI data and generate the required GMT files.
        //    All obsolete MGI terms as with any MGI terms that has 
        //    less than minSize and more than maxSize genes. In addition,
        //    we only keep gene sets that are up to 4 ontology level to avoid
        //    over specific gene sets 
        generate_mgi_sets(
            generate_ensembl_map.out.ensembl, 
            mgi_map, 
            mgi_obo, 
            mgi_rpt, 
            malacard,
            minSize, 
            maxSize)  
        // 4. Go through each MSigDB gmt file and filter out pathways that 
        //    have more than maxSize genes and less than minSize genes. Also
        //    translate the gene names into ensembl ID. 
        //    Here, we use the piping feature of nextflow so that we can 
        //    process each MSiGDB gmt files in parallel. What we did is for 
        //    each MSigDB file, we add the ensembl map, the malacard score and 
        //    also the size restriction, then we call the filter_msigdb_sets 
        //    process for "each" individual input
        msigdb \
            | combine(generate_ensembl_map.out.ensembl) \
            | combine(malacard) \
            | combine(minSize) \
            | combine(maxSize) \
            | filter_msigdb_sets
        // 5. Combine the gene set rankings and their corresponding gene set
        //    size into one file. Here we use mix, which append the results into
        //    a single vector. We then do something call groupTuple, which groups
        //    everything with the same first item (either ranking or count) together. 
        //    As a result of that, we can combine everything within the same group
        //    into one single file
        generate_go_sets.out.score \
            | mix(generate_go_sets.out.count) \
            | mix(generate_mgi_sets.out.score) \
            | mix(generate_mgi_sets.out.count) \
            | mix(filter_msigdb_sets.out.score) \
            | mix(filter_msigdb_sets.out.count) \
            | groupTuple(by: 0) \
            | combine_pathway_information
            
        // 6. Combine all GMTs into one object
        gmt = generate_go_sets.out.gmt \
            | mix(generate_mgi_sets.out.gmt) \
            | mix(filter_msigdb_sets.out.gmt)
    emit: 
        gmt = gmt
        ensembl = generate_ensembl_map.out.ensembl
        gtf = generate_ensembl_map.out.gtf
        ortholog = generate_mgi_sets.out.ortholog
}

workflow generate_specificity_sets{
    take: ensembl
    take: ortholog
    main:
        // Direct download of require files
        // 1. Download the median value from GTEx. Use Median as that's what's available and it will be 
        //    too time consuming for us to calculate the adjusted mean
        gtex_median=file("https://storage.googleapis.com/gtex_analysis_v8/rna_seq_data/GTEx_Analysis_2017-06-05_v8_RNASeQCv1.1.9_gene_median_tpm.gct.gz")
        // 2. Download the attribute file, which gives us the tissue information
        gtex_attribute=file("https://storage.googleapis.com/gtex_analysis_v8/annotations/GTEx_Analysis_v8_Annotations_SampleAttributesDS.txt")
        // 3. Generate GTEx gene sets. Genes were separated into each quantiles based on their expression specificity,
        //    calculated as the median expression in tissue / total expression across tissue
        generate_gtex_sets(
            gtex_median, 
            gtex_attribute,
            quantiles)
        // 4. Directly download the Specificity values calculated by Skene. 
        skene=file("https://static-content.springer.com/esm/art%3A10.1038%2Fs41588-018-0129-5/MediaObjects/41588_2018_129_MOESM3_ESM.xlsx")
        // 5. Process the Skene data. The main difficulty is to convert the mouse IDs to 
        //    the human ortholog
        generate_celltype_sets(
            skene, 
            ortholog, 
            ensembl,
            quantiles)
        gmt = generate_gtex_sets.out.gmt \
            | mix(generate_celltype_sets.out.gmt)
    emit:
        gmt
        
}

workflow generate_ldscores{
    take: gmt
    take: gtf
    main:
    // We need to generate the LD score estimates
    // 1. First, read in the 1000 genome phase 3 genotype data and filter out European samples
    // 2. Perform gene set annotation to generate the require format
    // 3. Run LD score calcuation in batch. 
    // 4. Split LD score of each gene set into an individual files so 
    //    that we can pack then into a single zip file
    // 5. Here we need some detail document: When we annotate the genome, we almost
    //    always have a Control set for each input. This cause problem where we will have 
    //    multiple file with the same name (Control). To counter this, did the following:
    // 6. Generate a channel containing the file name and the file path (first map)
    // 7. Group all file by their name. Any duplicated files will be grouped together(groupTuple)
    // 8. Retain only the first file in the list of duplicates
    // 9. Extract gene set name (last map)
    // 10.Group files by gene set name, then zip them
        reference \
            | combine(pop) \
            | genotype_filtering 
        geneSets = genotype_filtering.out.geno \
            | combine(gmt) \
            | combine(gtf) \
            | combine(Channel.of("${params.wind5}")) \
            | combine(Channel.of("${params.wind3}")) \
            | combine(Channel.of("${params.xregion}")) \
            | combine(Channel.of("${params.out}")) \
            | annotate_genome \
            | trim_annotation \
            | transpose
        beds = obtain_baseline_bed() \
            | flatten \
            | map{ a -> [ "tmp", a]} \
            | groupTuple \
            | map{ a -> [ a[1] ]}
        baselines = genotype_filtering.out.geno \
            | combine(beds) \
            | annotate_baseline_genome \
            | transpose \
            | update_baseline \
            | mix 
        setLDSC = geneSets \
            | mix(baselines) \
            | combine(genotype_filtering.out.geno, by: 0) \
            | combine(printsnp) \
            | calculate_ldscore \
            | filter{ a -> !(a[3] =~ /baseline/) } \
            | filter{ a -> !(a[3] =~ /weight/)} \
            | unwrap_ldscore \
            | flatten \
            | map{ a -> [   a.name, 
                            file(a)]} \
            | groupTuple(by: 0, sort: true) \
            | map{ a -> [ a[1][0]]} \
            | map{ a -> [   get_prefix(a[0].baseName.toString()),
                            a[0]]} \
            | groupTuple()
        // now group set based LDSC with the baseline and zip them
        calculate_ldscore.out \
            | filter{ a -> a[3] =~ /baseline/ || a[3] =~ /weight/ } \
            | rename_baseline_files \
            | flatten \
            | map{ a -> [   a.name, 
                            file(a)]} \
            | groupTuple(by: 0, sort: true) \
            | map{ a -> [ a[1][0]]} \
            | map{ a -> [   get_prefix(a[0].baseName.toString()),
                            a[0]]} \
            | groupTuple() \
            | mix(setLDSC) \
            | zip_ldsc
}