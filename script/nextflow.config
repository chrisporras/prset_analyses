process{
    // This are settings that are common to all jobs
    // Please change it to better suits your system
    executor ='lsf'
    clusterOptions = '-P acc_psychgen'    
    // for jobs that doesn't require much memory or process time
    withLabel: tiny{
        queue = 'express'
        cpus = '1'
        time = '30m'
        memory = '1G'
    }
    
    // for the most common type of jobs
    // use the less resource intensive queue
    // and only ask for one core and one hour of run time.
    // Also ask for 20G memory, which should generally be
    // more than enough for most processes
    withLabel: normal{
        queue = 'express'
        cpus = '1'
        time = '1h'
        memory = '20G'
    }
    // for jobs that require long time to complete
    withLabel: long{
        queue = 'express'
        cpus = '1'
        time = '12h'
        memory = '20G'
    }
    // for jobs that require medium time to complete
    withLabel: medium{
        queue = 'express'
        cpus = '1'
        time = '5h'
        memory = '20G'
    }
    
    // This is for memory intensive jobs. Similar to normal except
    // that we now ask for 40G of memory
    withLabel: more_mem{
        queue = 'express'
        cpus = '1'
        time = '1h'
        memory = '40G'
    }
    // This label is for LD score calculation, which can be resource
    // hungry and generate large number of file at the same time.
    // As such, we limit the number of jobs we run to 100
    withLabel: ldsc_calculation{
        memory = '40G'
        time = '10h'
        queue = 'express'
        cpus = '1'
        maxForks = '100'
    }
    // This label is tailor for LD score analysis in simulation, 
    // which due to large number of repeats, will submit unreasonable
    // amount of processes to server. Thus we generate a special for loop
    // to loop through multiple sets in one submission script
    withLabel: group_ldsc{
        memory = '8G'
        time = '3h'
        queue = 'express'
        cpus = '1'
        maxForks = '500'
    }
    // This label is for PRSet analysis that requires permutation.
    // Due the the computational burden, we need significantly higher
    // memory consumption and require multi-threading. Due to settings
    // of our server, we utilize the clusterOptions to better specify
    // the memory usage. For UK Biobank genotyped data, this job should
    // roughly require around 60GB of memory in total.
    withLabel: prset{
        cpus = '48'
        time = '24h'
        queue = 'premium'
        clusterOptions = '-P acc_psychgen -M 63219 -R select[mem>=2048] rusage[mem=2048]'
    }
}

executor{
    // This tell nextflow we are using a LSF system. 
    // You can change it to other executors:
    // https://www.nextflow.io/docs/latest/executor.html#lsf-executor
    // If you change this to `local`, you might want to add `maxForks` to the
    // processes to limit concurrent runs
    name='lsf'
    // Maximum number of jobs that can be submitted at the same time
    queueSize=700
}

// This enabled the use of singularity images
singularity{
    enable = true
    runOptions = "--no-home --cleanenv"
}
