#!/usr/bin/env nextflow

////////////////////////////////////////////////////////////////////
//
//    This script is to perform the best possible MAGMA analysis
//    for the supplementary results
//    We don't do the same for LDSC because that will require us 
//    to tailor LD score calculation for each trait and their
//    data, which we don't have the computation time for 
//
////////////////////////////////////////////////////////////////////


nextflow.enable.dsl=2
params.version=false
params.help=false
version='0.0.1'
timestamp='2021-04-13'
if(params.version) {
    System.out.println("")
    System.out.println("Run Best MAGMA Analysis - Version: $version ($timestamp)")
    exit 1
}


if(params.help){
    System.out.println("")
    System.out.println("Run Best MAGMA Analysis - Version: $version ($timestamp)")
    System.out.println("Usage: ")
    System.out.println("    nextflow run 2b_real_data_analysis_best_magma.nf [options]")
    System.out.println("Mandatory arguments:")
    System.out.println("    --loc         Gene location file for MAGMA")
    System.out.println("    --gmt         Directory containing the GMT files")
    System.out.println("    --gtf         GTF file for membership definition")
    System.out.println("    --bfile       Genotype file prefix ")
    System.out.println("    --ref         1000G reference for SNP coordinate mapping")
    System.out.println("    --fam         QCed fam file ")
    System.out.println("    --snp         QCed SNP file ")
    System.out.println("    --json        File containing all phenotype related information")
    System.out.println("    --cov         Covariate file")
    System.out.println("    --dropout     dropout samples")
    System.out.println("    --sql         UK biobank SQL data base")
    System.out.println("    --gtex        Cell type specificity file")
    System.out.println("    --malacard    Malacard score for gene sets")
    System.out.println("    --gene        Malacard score of individual gene")
    System.out.println("    --expert      CSV containing expert opion")
    System.out.println("Options:")
    System.out.println("    --wind3       Padding to 3' end ")
    System.out.println("    --wind5       Padding to 5' end")
    System.out.println("    --help        Display this help messages")
} 


////////////////////////////////////////////////////////////////////
//                  Helper Functions
////////////////////////////////////////////////////////////////////
def fileExists(fn){
   if (fn.exists()){
       return fn;
   }else
       error("\n\n-----------------\nFile $fn does not exist\n\n---\n")
}

def gen_file(a, bgen){
    return bgen.replaceAll("@",a.toString())
}

def get_chr(a, input){
    if(input.contains("@")){
        return a
    }
    else {
        return 0
    }
}


// Trying to parse the JSON input
import groovy.json.JsonSlurper
def jsonSlurper = new JsonSlurper()
// new File object from your JSON file
def ConfigFile = new File("${params.json}")
// load the text from the JSON
String ConfigJSON = ConfigFile.text
// create a dictionary object from the JSON text
def phenoConfig = jsonSlurper.parseText(ConfigJSON)
////////////////////////////////////////////////////////////////////
//                  Module inclusion
////////////////////////////////////////////////////////////////////
include {   standardize_summary_statistic
            filter_summary_statistic
            rename_sumstat
            filter_bed
            perform_gwas
            meta_analysis
            modify_metal    }   from './modules/basic_genetic_analyses'
include {   malacards_score_analysis
            prep_null_for_perm
            perform_perm_kendall
            combine_kendall_null
            generate_ensembl_map    }   from './modules/generate_biochemical_sets'
include {   specificity_analysis  }   from './modules/generate_specificity_sets'
include {   extract_phenotype_from_sql
            residualize_phenotypes  }   from './modules/phenotype_extraction'
include {   combine_gmt
            group_bim
            mock_qc
            mock_file }   from './modules/misc'
include {   get_overlap_snps
            perform_annotation
            magma_genotype
            magma_sumstat
            magma_meta
            magma_gene_set
            magma_specificity
            modify_magma_output}   from './modules/magma'
////////////////////////////////////////////////////////////////////
//                  Setup Channels
////////////////////////////////////////////////////////////////////

biochemGMT = Channel.fromPath("${params.gmt}/*.gmt") 
specMAGMA=Channel.fromPath("${params.gtex}/*magma")
specGMT=Channel.fromPath("${params.gtex}/*gmt")

genotype = Channel.fromFilePairs("${params.bfile}.{bed,bim,fam}", size:3 , flat: true){ file -> file.baseName } \
    | ifEmpty{ error "No matching plink files "} \
    | map{ a -> [ fileExists(a[1]), fileExists(a[2]), fileExists(a[3])]}


sql = Channel.fromPath("${params.sql}")
dropout = Channel.fromPath("${params.dropout}")
qcFam = Channel.fromPath("${params.fam}")
snp = Channel.fromPath("${params.snp}")
wind3 = Channel.of("${params.wind3}")
wind5 = Channel.of("${params.wind5}")
cov = Channel.fromPath("${params.cov}")

pheno = Channel.from(phenoConfig.collect{ content ->
    [   content.name, 
        file(content.phenoFile), 
        file(content.rscript)
    ]})
geneLoc=Channel.fromPath("${params.loc}")
chromosome = Channel.value(1..22).flatten()      
reference = chromosome \
    | map{ a -> [   fileExists(file(gen_file(a, "${params.ref}.bed"))),
                    fileExists(file(gen_file(a, "${params.ref}.bim"))),
                    fileExists(file(gen_file(a, "${params.ref}.fam")))]}
                    
sumstat = Channel.from(phenoConfig.collect{ content ->
    [   content.name, 
        file(content.gwas), 
        content.rsid,
        content.a1,
        content.a2,
        content.statistic,
        content.pvalue,
        content.se,
        content.ncol,
        content.sampleSize,
        content.isBeta
    ]})
xregions = Channel.from(phenoConfig.collect{ content ->
    [   content.name,
        content.xregion
    ]})

malacard = Channel.fromPath("${params.malacard}")
gene = Channel.fromPath("${params.gene}")
expert = Channel.fromPath("${params.expert}")
gtf = Channel.fromPath("${params.gtf}")

workflow{
    combine_gmt(biochemGMT.collect(), specGMT.collect())
    phenotype_preprocessing()
    magma_analyses(
        phenotype_preprocessing.out,
        combine_gmt.out)
    downstream_analyses(
        magma_analyses.out,
        combine_gmt.out
    )
}


workflow phenotype_preprocessing{
    // 1. Extract Phenotype from SQL 
    pheno \
        | filter{ a -> a[0] != "SCZ"} \
        | combine(sql) \
        | extract_phenotype_from_sql 
    // 1. Or in the case of PGC SCZ, the phenotype file is directly provided
    //    Noted: Because we SCZ have a completely different set of input, we
    //    have to run it separately (and have a mock dropout file)
    pheno \
        | filter{ a -> a[0] == "SCZ"} \
        | map{  a -> [  a[0],   // phenotype name
                        a[2],   // Rscript
                        a[1]]   /* Phenotype file */} \
        | mix(extract_phenotype_from_sql.out ) \
        | combine(qcFam) \
        | combine(dropout) \
        | combine(cov) \
        | residualize_phenotypes
    emit:
        residualize_phenotypes.out
    // Main reason why we ask the script to extract and residualize the phenotype
    // is such that we maximize the number of steps that are performed within the
    // pipeline, thus reduce manual processing of data (e.g. previous scheme where
    // the residualized phenotype is manually extracted before running the pipeline)
}



workflow magma_analyses{
    take: pheno
    take: gmt
    main:
        // 1. Generate the required annotation file for MAGMA
        // Manually group the bim file. Will also generate an 
        // empty bed and fam file so that we can reuse channel
        group_bim(reference.collect())
        // duplicate the bim file so that when we do filtering, 
        // we keep all SNPs from the 1000 genome
        mock_qc(group_bim.out)
        sumstat \
            | standardize_summary_statistic
            | combine(mock_qc.out) \
            | combine(group_bim.out) \
            | combine(xregions, by: 0) \
            | combine(Channel.of("F")) /*Don't filter out ambiguous SNPs*/\
            | filter_summary_statistic
        filter_summary_statistic.out.snp \
            | combine(group_bim.out) \
            | get_overlap_snps \
            | combine(geneLoc) \
            | combine(wind5) \
            | combine(wind3) \
            | perform_annotation
        // 2. Run MAGMA on the target genotype in order to obtain the required
        //    gene based statistic
        
        pheno \
            | combine(perform_annotation.out, by: 0) \
            | combine(genotype) \
            | magma_genotype
        // 3. Perform MAGMA analysis on the base summary statistic (genotype is used for 
        //    LD calculation)
        filter_summary_statistic.out.sumstat \
            | combine(perform_annotation.out, by: 0) \
            | combine(genotype) \
            | magma_sumstat
        // 4. Meta analyzed the base and target results
        magma_sumstat.out \
            | combine(magma_genotype.out, by: 0) \
            | magma_meta
        // 5. Perform set based MAGMA analysis on the biochemical pathways
        //    and the specificity pathways
        magma_meta.out \
            | combine(gmt) \
            | magma_gene_set
        // 6. Also perform the cell type specificity analysis developed by MAGMA
        //    as a sanity check
        specMAGMA \
            | combine(magma_meta.out) \
            | magma_specificity
        
        // 7. Harmonize MAGMA output
        magma_specificity.out \
            | groupTuple(by: [0,1]) \
            | mix(magma_gene_set.out) \
            | modify_magma_output 
    emit:
        modify_magma_output.out
}

workflow downstream_analyses{
    take: magmaRes
    take: gmt
    main:
        // filter wasn't require in the other script because we always merge with 
        // PRSet, which does not have a type of "Specificity"
        
        // reason for mocking two empty file is such that we can directly reuse 
        // the analysis process used in the main pipeline, which included results 
        // from LDSC and prset
        mock_input = Channel.of("prset", "ldsc", "ldspec") \
            | mock_file \
            | toSortedList \
            | map{ a -> [   a[2],
                            a[1],
                            a[0]]}
        
        resultGroup = magmaRes \
            | combine(mock_input) \
            | map{ a -> [   a[0],   // Phenotype
                            a[1],   // Type
                            a[3],   // PRSet
                            a[2],   // MAGMA
                            a[4]]}  /* LDSC */

       resultGroup \
            | filter{ a -> a[1] != "Specificity"} \
            | combine(malacard) \
            | malacards_score_analysis

        generate_ensembl_map(gtf)
        resultGroup \
            | filter{ a -> a[1] != "Specificity"} \
            | combine(generate_ensembl_map.out.ensembl) \
            | combine(gmt) \
            | combine(gene) \
            | combine(Channel.of(100)) \
            | prep_null_for_perm \
            | combine(Channel.of(10)) \
            | combine(Channel.of(1..10)) \
            | perform_perm_kendall \
            | groupTuple \
            | combine_kendall_null
        // there are two type of LDSC and MAGMA cell type specificity output, 
        // we will still try to handle them here, but will only include them
        // in supplementary as that is a lot
        spec = resultGroup \
            | combine(mock_input) \
            | filter{ a -> a[1] == "Specificity"} \
            | map{  a -> [  a[0],
                            a[3],
                            a[7]]} 

       resultGroup \
            | filter { a -> a[1] != "Specificity"} \
            | combine(spec, by: 0) \
            | groupTuple(by: [0,1]) 
            | combine(expert) \
            | specificity_analysis

}