process generate_gtex_sets{
    // prepare the gtex data for tissue specificity processing. Generate required matrix for MAGMA
    // and gmt for LDSC and PRSet
    
    label 'more_mem'
    publishDir "data/gene_sets/celltype", mode: 'copy', overwrite: true
    input:
        path(median)
        path(attribute)
        val(quantiles)
    output:
        path "GTEx.gmt", emit: gmt
        path "GTEx.magma", emit: magma
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    # Function to help calculating the quantiles
    get_quantile <- function(x, num.quant, quant.ref=NA) {
      breaks <- x %>%
        quantile(., probs = seq(0, 1, 1 / num.quant), na.rm=T) %>%
        unique
      quant <- x %>% 
        cut(., breaks = breaks, include.lowest=T) %>%
        as.numeric
      if (is.na(quant.ref) | is.null(quant.ref)) {
        quant.ref <- ceiling(num.quant / 2)
      }
      quant %>% 
        factor(., levels = c(quant.ref, seq(min(., na.rm=T), max(.,na.rm=T), 1)[-quant.ref])) %>%
        return
      return(quant)
    }
    
    # Remove special characters and spaces from the colnames so that it is easier to work with
    gtex.median <- fread("${median}") %>%
      setnames(., colnames(.), sapply(colnames(.), function(i) {
        gsub("- ", "", i) %>%
          gsub(" ", "_", .) %>%
          gsub(":", "_", .) %>%
          gsub("-", "_", .) %>%
          gsub("\\\\(", "", .) %>%
          gsub("\\\\)", "", .) %>%
          gsub("_+","_", .)
      }))
    # Read in the tissue information. Update the colnames so that they have the same name as the 
    # median
    gtex.sample <- fread("${attribute}") %>%
      .[, c("SAMPID", "SMTS", "SMTSD")] %>%
      .[, SMTSD := sapply(SMTSD, function(i) {
        gsub("- ", "", i) %>%
          gsub(" ", "_", .) %>%
          gsub(":", "_", .) %>%
          gsub("-", "_", .) %>%
          gsub("\\\\(", "", .) %>%
          gsub("\\\\)", "", .)
      })]
    # Remove tissues with less than 100 data point.
    # Also remove cancer tissues and Testis, which is an outlier
    valid.tissue <- gtex.sample %>% 
      .[,.(SMTS=unique(SMTS), N=.N), by=SMTSD] %>%
      .[N>100] %>%
      .[!grepl("ukemia|lympocytes|Testis", SMTSD)] %>%
      unique %>%
      .[,SMTSD]
    quant <- ${quantiles}
    # Here, we follow procedure of Skene:
    # 1. Scale the expression of each tissue such that total is 1,000,000
    #    This is achieved using the .SDcols and .SD feature of data.table.
    #    .SDcols help us to select all tissues (therefore ignoring the 
    #    gene name and gene symbols) and then apply the scaling function
    #    to each column (lapply)
    # 2. Calculate the rowSums. Again, use .SDcols to ignore the gene name
    #    and symbol
    # 3. Remove genes that are not expressed anywhere
    # 4. Get the relative specificity by dividing the tissue median by the total
    #    expression
    # 5. Remove the SUM column as we no longer require it
    # 6. Update the ensembl ID by stripping the version identifier, which is not
    #    presented in our GTF file
    # 7. Extract the valid tissues and the ensembl IDs
    # 8. If specificity is 0, change it to NA so that they will be ignored when assigning quantiles
    # 9. Assign quantiles and replace specificity
    # 10. Convert the character quantiles into numeric value and assign back the zeros
    gtex.scaled <- gtex.median %>%
      .[, (valid.tissue) := lapply(.SD, function(x) {
        return(x * 1000000 / sum(x))
      }), .SDcols = valid.tissue] %>%
      .[, SUM := rowSums(.SD), .SDcols = valid.tissue] %>%
      .[SUM > 0] %>%
      .[, (valid.tissue) := .SD / SUM, .SDcols = valid.tissue] %>%
      .[, SUM := NULL] %>%
      # Remove SUM and also rename the gene to remove the version identifier which is not presented in our gtf
      .[, Name := lapply(Name, function(x) {
        strsplit(x, split = "\\\\.") %>%
          unlist %>%
          head(n = 1)
      })] %>%
      .[, c("Name", valid.tissue), with = F] %>%
      .[, (valid.tissue) := lapply(.SD, function(x) {
        x[x == 0] <- NA
        x
      }), .SDcols = valid.tissue] %>%
      .[, (valid.tissue) := lapply(.SD, get_quantile, quant, 1), .SDcols = valid.tissue] %>%
      .[, (valid.tissue) := lapply(.SD, function(x) {
        x %<>%
          as.character %>%
          as.numeric
        x[is.na(x)] <- 0
        x
      }), .SDcols = valid.tissue]
    # Function for generating the GMT file. 
    # We put it into function just to make it clearer what we are doing
    generate_gmt <- function(input, name, quantile){
        tissue <- colnames(input[, -c("Name")])
        fileConn <- file(name, open = "wt")
        # Go through each tissue
        for (i in tissue) {
            tissue.name <- i
            for (j in 0:quant) {
                input[get(i)==j, Name] %>%
                    paste(., collapse="\\t",sep="\\t") %>%
                    paste(paste(tissue.name, j, sep=":"), .)  %>%
                    writeLines(., fileConn)
                }
            }
        close(fileConn)
    }
    generate_gmt(gtex.scaled, "GTEx.gmt", quant=quant)
    # For MAGMA, it requires a matrix for its specificity function
    # Though we are not looking at that for now as we can't do a like to 
    # like comparison to LDSC and PRSet
    fwrite(gtex.scaled, "GTEx.magma", sep="\\t", na="NA", quote=F)
    """
}

process generate_celltype_sets{
    
    label 'normal'
    publishDir "data/gene_sets/celltype", mode: 'copy', overwrite: true
    input:
        path(skene)
        path(ortholog)
        path(ensembl)
        val(quantiles)
    output:
        path "skene.gmt", emit: gmt
        path "skene.magma", emit: magma
    script:
    """
    #!/usr/bin/env Rscript
    library(readxl)
    library(magrittr)
    library(data.table)
    # Function to help calculating the quantiles
    get_quantile <- function(x, num.quant, quant.ref=NA) {
      breaks <- x %>%
        quantile(., probs = seq(0, 1, 1 / num.quant), na.rm=T) %>%
        unique
      quant <- x %>% 
        cut(., breaks = breaks, include.lowest=T) %>%
        as.numeric
      if (is.na(quant.ref) | is.null(quant.ref)) {
        quant.ref <- ceiling(num.quant / 2)
      }
      quant %>% 
        factor(., levels = c(quant.ref, seq(min(., na.rm=T), max(.,na.rm=T), 1)[-quant.ref])) %>%
        return
      return(quant)
    }
    # Read in Excel from the supplementary data
    # This file contains the pre-calculated cell type
    # expression specificity. All we need to do is to 
    # convert the gene symbols to its ortholog and calculate
    # the quantiles
    # Make sure we remove all special characters from 
    # the cell type names to make it easier to work with
    skene <- read_excel("${skene}")%>%
      as.data.table %>%
      setnames(., colnames(.)[1], "Gene") %>%
      setnames(., colnames(.), sapply(colnames(.), function(i) {
        gsub("- ", "", i) %>%
          gsub(" ", "_", .) %>%
          gsub(":", "_", .) %>%
          gsub("-", "_", .) %>%
          gsub("\\\\(", "", .) %>%
          gsub("\\\\)", "", .) %>%
          gsub("_+", "_", .)
      }))
      
    human.mouse <- fread("${ortholog}")
    ensembl <- fread("${ensembl}")[,c("Name", "ID")]
    skene.human <- merge(skene, human.mouse, by.x = "Gene", by.y = "Mouse") %>%
        merge(ensembl, by.x="Human", by.y="Name")
    # We want to remove multi-mapped Genes to avoid bias
    # Then we remove un-necessary columns
    duplicated.map <- skene.human[duplicated(ID) | duplicated(Gene), ID]
    skene.human %<>% 
        .[!ID %in% duplicated.map] %>%
        .[, -c("Gene", "Human")] %>%
        setnames(., "ID", "Gene")
    tissue.name <- colnames(skene.human[,-c("Gene")])
    # Now calculate the quantiles. For detail, refer to the GTEx function
    quant <- ${quantiles}
    skene.quant <- skene.human %>%
      .[, (tissue.name) := lapply(.SD, function(x) {
        x[x == 0] <- NA
        x
      }), .SDcols = tissue.name] %>%
      .[, (tissue.name) := lapply(.SD, get_quantile, quant, 1), .SDcols = tissue.name] %>%
      .[, (tissue.name) := lapply(.SD, function(x) {
        x %<>%
          as.character %>%
          as.numeric
        x[is.na(x)] <- 0
        x
      }), .SDcols = tissue.name]
    
     # Function for generating the GMT file. 
    # We put it into function just to make it clearer what we are doing
    generate_gmt <- function(input, name, quantile){
        tissue <- colnames(input[, -c("Gene")])
        fileConn <- file(name, open = "wt")
        # Go through each tissue
        for (i in tissue) {
            tissue.name <- i
            for (j in 0:quant) {
                input[get(i)==j, Gene] %>%
                    paste(., collapse="\\t",sep="\\t") %>%
                    paste(paste(tissue.name, j, sep=":"), .)  %>%
                    writeLines(., fileConn)
                }
            }
        close(fileConn)
    }
    
    generate_gmt(skene.quant, "skene.gmt",quantile=quant)
    
    # For MAGMA, it requires a matrix for its specificity function
    # Though we are not looking at that for now as we can't do a like to 
    # like comparison to LDSC and PRSet
    # Remember to reorganize the matrix so that gene name come first
    fwrite(skene.quant[,c("Gene", tissue.name), with=F], "skene.magma", sep="\\t", na="NA", quote=F)
    """
}


process specificity_analysis{
    label 'normal'
    publishDir "result", mode: 'copy', overwrite: true
    input:
        tuple   val(pheno),
                val(type),
                path(prset),
                path(magma),
                path(ldsc),
                path(magmaSpec),
                path(ldscSpec),
                path(expert)
    output:
        tuple   val(pheno),
                path("${pheno}-celltype.csv"),
                path("${pheno}-celltype-raw.csv")
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    res <- fread("${prset}") %>%
        rbind(., fread("${magma}"), fill=T) %>%
        rbind(., fread("${magmaSpec}"), fill=T) %>%
        rbind(., fread("${ldsc}"), fill=T) %>%
        rbind(., fread("${ldscSpec}"), fill=T)
    # Extract the specificity results
    res <- res[, Quantiles := sapply(Set, function(x){
            strsplit(x, split=":") %>%
            unlist %>%
            tail(n=1) %>%
            as.numeric
        })] %>%
        .[!is.na(Quantiles)] %>%
        .[ !(Set %like% "^GO|^MP")] %>%
        .[, Set := sapply(Set, function(x){
            strsplit(x, split=":") %>%
            unlist %>%
            head(n=1)
        })]

    # Calculate p-value for linear relationship and extract 
    # p-value from largest quantile
    celltype <- res %>% 
        .[, logP := -log10(P)] %>%
        .[Coefficient < 0 & !(Type=="Specificity" & Software=="MAGMA"), logP := abs(logP)] %>%
        .[, {
            model <- lm(logP ~ Quantiles) 
            beta <- coef(summary(model))[2,3]
            maxQuant <- .SD[Quantiles == max(Quantiles)] %>%
                .[!(Type=="Specificity" & Software=="MAGMA") & Coefficient < 0, P := 1 - P / 2]
            list(
                Coefficient = c(beta,
                    maxQuant[,Coefficient]),
                P = c(pt(beta, model\$df, lower.tail=F),
                    maxQuant[,P]),
                Algorithm = c("lm signed P on quantile (one sided)",
                "Top Quantile")
            )
        }, by = c("Set", "Software", "Type")] %>%
        setnames(., "Type", "Analysis")
    fwrite(celltype, "${pheno}-celltype-raw.csv")
    # Again, coding of SCZ is wrong
    expert <- fread("${expert}")
    select <- grepl(paste0("^${pheno}"), colnames(expert))
    if("${pheno}" == "SCZ"){
        select <- grepl(paste0("^scz"), colnames(expert))
    }
    select[c(2, 3)] <- T
    expert.curate <- expert[, ..select] %>%
            melt(., id.var = c("Type", "Detail")) %>%
            .[is.na(value), value := 0] %>%
            .[, value := sapply(value, function(x, y) {
                # If experts provide us with ranking, only consider the top 25
                # tissues as true (AFAIK, all expert has less than 25)
                if (x == 0)
                    return(x)
                if (max(y, na.rm = T) > 2) {
                    return(as.numeric(x < 25))
                } else{
                    return(as.numeric(x < 2))
                }
            }, value)] %>%
            .[, .(Score = sum(value)), by = c("Detail", "Type")] %>%
            .[, Detail := sapply(Detail, function(i) {
                gsub("- ", "", i) %>%
                    gsub(" ", "_", .) %>%
                    gsub(":", "_", .) %>%
                    gsub("-", "_", .) %>%
                    gsub("\\\\(", "", .) %>%
                    gsub("\\\\)", "", .) %>%
                    gsub("_+", "_", .)
            })] %>%
        setnames(., "Detail", "Set") %>%
        setnames(., "Type", "Data.Source")
    # Remove the whole analysis (usually cell type) if there isn't any data
    score.count <- expert.curate %>%
        .[,.(Sum=sum(Score)), by=c("Data.Source")] 
    expert.curate %<>% .[!Data.Source %in% score.count[Sum==0, Data.Source]]
        
    lmp <- function (modelobject) {
        if (!inherits(modelobject, "lm"))
            stop("Not an object of class 'lm' ")
        f <- summary(modelobject)\$fstatistic
        p <- unname(pf(f[1], f[2], f[3], lower.tail = F))
        return(p)
    }
    inverse.normal <- function(x) {
        qnorm((rank(x, na.last = "keep") - 0.5) / sum(!is.na(x)))
    }
    # Now perform factor analysis
    factor.lm <- celltype %>%
        merge(., expert.curate) %>%
        .[, Score := as.factor(Score)] %>%
        .[, logP := inverse.normal(-log10(P)), by = c("Software", "Analysis", "Algorithm")] %>%
        .[, {
            model <- lm(formula = logP ~ Score, data = .SD)
            fstat <- summary(model)\$fstatistic
            coef <- model %>% summary %>% coef
            list(
                P.full = lmp(model),
                P.1 = ifelse(
                    "Score1" %in% row.names(coef),
                    ifelse(coef["Score1", "Estimate"] > 0,
                           coef["Score1", "Pr(>|t|)"], 1),
                    as.numeric(NA)
                ),
                Beta.1 = ifelse("Score1" %in% row.names(coef),
                                coef["Score1", "Estimate"],
                                as.numeric(NA)),
                SE.1 = ifelse("Score1" %in% row.names(coef),
                              coef["Score1", "Std. Error"],
                              as.numeric(NA)),
                P.2 = ifelse(
                    "Score2" %in% row.names(coef),
                    ifelse(coef["Score2", "Estimate"] > 0,
                           coef["Score2", "Pr(>|t|)"], 1),
                    as.numeric(NA)
                ),
                Beta.2 = ifelse("Score2" %in% row.names(coef),
                                coef["Score2", "Estimate"],
                                as.numeric(NA)),
                SE.2 = ifelse("Score2" %in% row.names(coef),
                                coef["Score2", "Std. Error"],
                                as.numeric(NA)),
                f.stat = fstat[1],
                df = fstat[3]
            )
        }, by = c("Software", "Analysis", "Algorithm", "Data.Source")] %>%
        .[order(P.full)]
    fwrite(factor.lm, "${pheno}-celltype.csv")
    """
}
