process filter_bed{
    label 'normal'
    input: 
        tuple   val(name),
                path(snp),
                path(pheno),
                path(bed),
                path(bim), 
                path(fam)
    output:
        tuple   val(name),
                path("${name}-qc.bed"),
                path("${name}-qc.bim"), 
                path("${name}-qc.fam")
    script:
    base=bed.baseName
    """
    plink \
        --bfile ${base} \
        --extract ${snp} \
        --keep ${pheno} \
        --out ${name}-qc \
        --make-bed
    """
}

process rename_sumstat{
    module 'R/4.0.3'
    time '1h'
    memory '20G'
    executor 'lsf'
    queue 'express'
    input:
      tuple   val(pheno),
              path(sumstat),
              val(rs),
              val(a1),
              val(a2),
              val(stat),
              val(p),
              val(se),
              val(ncol),
              val(n),
              val(beta),
              val(xregion),
              path(bed),
              path(bim),
              path(fam),
              val(tmp),
              path("*"),
              path("*"),
              path("*")
    output:
      // from this point onward, the sumstat file is standardized
        tuple   val(pheno),
                path("${pheno}-snp.loc"), emit: annot
        tuple   val(pheno),
                path("${pheno}-overlap.sumstat.gz"), emit: gwas
    script:
    """ 
    #!/usr/bin/env Rscript
    library(data.table, lib.loc = "/hpc/users/chois26/.Rlib")
    library(magrittr)
    bimFiles <- list.files(pattern="bim\$")
    bim <- NULL
    for(i in bimFiles){
        bim %<>% rbind(., fread(i))
    }
    bim %<>%
        .[, c("V1", "V2", "V4")] %>%
        unique
    # First, remove potential duplicates
    sumstat <- fread("${sumstat}")
    if("${rs}" != "SNP" && "SNP" %in% colnames(sumstat)){
        sumstat <- sumstat[,-c("SNP")]
    }
    if("${a1}" != "A1" && "A1" %in% colnames(sumstat)){
        sumstat <- sumstat[,-c("A1")]
    }
    if("${a2}" != "A2" && "A2" %in% colnames(sumstat)){
        sumstat <- sumstat[,-c("A2")]
    }
    if("${stat}" != "BETA" && "BETA" %in% colnames(sumstat)){
        sumstat <- sumstat[,-c("BETA")]
    }
    if("${p}" != "P" && "P" %in% colnames(sumstat)){
        sumstat <- sumstat[,-c("P")]
    }
    setnames(sumstat, "${rs}", "SNP")
    setnames(sumstat, "${a1}", "A1")
    setnames(sumstat, "${a2}", "A2")
    setnames(sumstat, "${stat}", "BETA")
    setnames(sumstat, "${p}", "P")
    if ("${beta}" != "true") {
        sumstat[, BETA := log(BETA)]
    }
    if ("${ncol}" == "null") {
        sumstat[, N := ${n}]
    } else{
        setnames(sumstat, "${ncol}", "N")
    }
    xregion <- "${xregion}" %>%
        strsplit(., split = ",") %>%
        unlist
    for (region in xregion) {
        if (region != "") {
            breakdown <- strsplit(region, ":") %>%
                unlist
            range <- breakdown %>%
                tail(n = 1) %>%
                strsplit(., "-") %>%
                unlist %>%
                as.numeric
            chr <- breakdown %>%
                head(n = 1) %>%
                gsub("chr", "", .) %>%
                as.numeric
            # Filter based on bim, as bim always contain all required information
            bim <-
                bim[!(V1 == chr & V4 >= range[1] & V4 <= range[2])]
        }
    }
    bim %>% 
        .[, c("V2", "V1", "V4")] %>%
        fwrite(., "${pheno}-snp.loc", quote=F, row.names=F, col.names=F, sep="\\t")
    # It is way easier just to map the sumstat to 1000 genome + UKB instead of 
    # trying to conditionally extract the column names
    # Given the density of 1000 genome reference, we should be ok
    sumstat[SNP %in% bim[, V2]] %>%
        fwrite(
            .,
            "${pheno}-overlap.sumstat.gz",
            quote = F,
            na = "NA",
            sep = "\\t"
        )
    """
}

process filter_summary_statistic{
    label 'normal'
    // This process will filter summary statistics based on user input.
    // If xregion is not null, the regions indicated by xregion will be
    // removed. 
    // ambiguous SNPs will be removed depending on rmAmbig, which is
    // assumed to be T or F
    input:
        tuple   val(pheno),
                path(sumstat),
                path(snp),
                path(bed),
                path(bim),
                path(fam),
                val(xregion),
                val(rmAmbig)
    output:
        tuple   val(pheno),
                path("${pheno}-overlap.snp"), emit: snp
        tuple   val(pheno),
                path("${pheno}-overlap.sumstat.gz"), emit: sumstat
    script:
    """
    #!/usr/bin/env Rscript
    library(data.table)
    library(magrittr)
    ambig <- function(x, y) {
        if (x == "A" & y == "T")
            return(T)
        if (x == "T" & y == "A")
            return(T)
        if (x == "C" & y == "G")
            return(T)
        if (x == "G" & y == "C")
            return(T)
        return(F)
    }
    bim <- fread("${bim}", header = F)
    snps <- fread("${snp}", header = F)
    # Filter out post QCed SNPs
    bim %<>% .[V2 %in% snps[, V1]]
    if(${rmAmbig}){
        bim %<>%
            .[, Ambig := lapply(.SD, function(x) {
                    ambig(V5, V6)
                }), .SDcols = c("V5", "V6"), by = "V2"] %>%
            .[Ambig == F] %>%
            .[, -c("Ambig")]
    }
    if("${xregion}" != "null"){
        xregion <- "${xregion}" %>%
            strsplit(., split = ",") %>%
            unlist
        for (region in xregion) {
            if (region != "") {
                breakdown <- strsplit(region, ":") %>%
                    unlist
                range <- breakdown %>%
                    tail(n = 1) %>%
                    strsplit(., "-") %>%
                    unlist %>%
                    as.numeric
                chr <- breakdown %>%
                    head(n = 1) %>%
                    gsub("chr", "", .) %>%
                    as.numeric
                # Filter based on bim, as bim always contain all required information
                bim <-
                    bim[!(V1 == chr & V4 >= range[1] & V4 <= range[2])]
            }
        }
    }
    sumstat <- fread("${sumstat}")
    overlap <- bim[V2 %in% sumstat[, SNP]]
    sumstat[SNP %in% overlap[, V2]] %>%
        fwrite(
            .,
            "${pheno}-overlap.sumstat.gz",
            quote = F,
            na = "NA",
            sep = "\\t"
        )
    write.table(overlap, "${pheno}-overlap.snp", quote=F, row.names=F, col.names=F)
    """

}
process standardize_summary_statistic{
    label 'normal'
    input:
        tuple   val(pheno),
                path(sumstat),
                val(rs),
                val(a1),
                val(a2),
                val(stat),
                val(p),
                val(se),
                val(ncol),
                val(n),
                val(beta)
    output:
      // from this point onward, the sumstat file is standardized
        tuple   val(pheno),
                path("${pheno}-std.sumstat.gz")
    script:
    """
    #!/usr/bin/env Rscript
    library(data.table)
    library(magrittr)
    sumstat <- fread("${sumstat}")
    rmDupCol <- function(x, y, input){
        if(x != y & y %in% colnames(input)){
            return(input[, -c(y), with=F])
        }
    }
    rmDupCol("${rs}", "SNP", sumstat)
    rmDupCol("${a1}", "A1", sumstat)
    rmDupCol("${a2}", "A2", sumstat)
    rmDupCol("${stat}", "BETA", sumstat)
    rmDupCol("${p}", "P", sumstat)
    rmDupCol("${ncol}", "N", sumstat)
    setnames(sumstat, "${rs}", "SNP")
    setnames(sumstat, "${a1}", "A1")
    setnames(sumstat, "${a2}", "A2")
    setnames(sumstat, "${stat}", "BETA")
    setnames(sumstat, "${p}", "P")
    if ("${beta}" != "true") {
        sumstat[, BETA := log(BETA)]
    }
    if ("${ncol}" == "null") {
        sumstat[, N := ${n}]
    } else{
        setnames(sumstat, "${ncol}", "N")
    }
    fwrite(
        sumstat,
        "${pheno}-std.sumstat.gz",
        quote = F,
        na = "NA",
        sep = "\\t"
    )
    """

}
process modify_summary_statistic{
    module 'R/4.0.3'
    time '1h'
    memory '20G'
    executor 'lsf'
    queue 'express'
    input:
      tuple   val(pheno),
              path(sumstat),
              val(rs),
              val(a1),
              val(a2),
              val(stat),
              val(p),
              val(se),
              val(ncol),
              val(n),
              val(beta),
              val(xregion),
              path(snp),
              path(bed),
              path(bim),
              path(fam)
    output:
      // from this point onward, the sumstat file is standardized
      tuple   val(pheno),
              path("${pheno}-overlap.snp"),
              path("${pheno}-overlap.sumstat.gz")
    script:
    """ 
    #!/usr/bin/env Rscript
    library(data.table, lib.loc = "/hpc/users/chois26/.Rlib")
    library(magrittr)
    bim <- fread("${bim}", header = F)
    snps <- fread("${snp}", header = F)
    ambig <- function(x, y) {
        if (x == "A" & y == "T")
            return(T)
        if (x == "T" & y == "A")
            return(T)
        if (x == "C" & y == "G")
            return(T)
        if (x == "G" & y == "C")
            return(T)
        return(F)
    }
    bim %<>% .[V2 %in% snps[, V1]] %>%
        .[, Ambig := lapply(.SD, function(x) {
            ambig(V5, V6)
        }), .SDcols = c("V5", "V6"), by = "V2"] %>%
        .[Ambig == F] %>%
        .[, -c("Ambig")]
    # First, remove potential duplicates
    
    sumstat <- fread("${sumstat}")
    if("${rs}" != "SNP" && "SNP" %in% colnames(sumstat)){
        sumstat <- sumstat[,-c("SNP")]
    }
    if("${a1}" != "A1" && "A1" %in% colnames(sumstat)){
        sumstat <- sumstat[,-c("A1")]
    }
    if("${a2}" != "A2" && "A2" %in% colnames(sumstat)){
        sumstat <- sumstat[,-c("A2")]
    }
    if("${stat}" != "BETA" && "BETA" %in% colnames(sumstat)){
        sumstat <- sumstat[,-c("BETA")]
    }
    if("${p}" != "P" && "P" %in% colnames(sumstat)){
        sumstat <- sumstat[,-c("P")]
    }
    setnames(sumstat, "${rs}", "SNP")
    setnames(sumstat, "${a1}", "A1")
    setnames(sumstat, "${a2}", "A2")
    setnames(sumstat, "${stat}", "BETA")
    setnames(sumstat, "${p}", "P")
    if ("${beta}" != "true") {
        sumstat[, BETA := log(BETA)]
    }
    if ("${ncol}" == "null") {
        sumstat[, N := ${n}]
    } else{
        setnames(sumstat, "${ncol}", "N")
    }
    xregion <- "${xregion}" %>%
        strsplit(., split = ",") %>%
        unlist
    for (region in xregion) {
        if (region != "") {
            breakdown <- strsplit(region, ":") %>%
                unlist
            range <- breakdown %>%
                tail(n = 1) %>%
                strsplit(., "-") %>%
                unlist %>%
                as.numeric
            chr <- breakdown %>%
                head(n = 1) %>%
                gsub("chr", "", .) %>%
                as.numeric
            # Filter based on bim, as bim always contain all required information
            bim <-
                bim[!(V1 == chr & V4 >= range[1] & V4 <= range[2])]
        }
    }
    overlap <- bim[V2 %in% sumstat[, SNP]]
    
    sumstat[SNP %in% overlap[, V2]] %>%
        fwrite(
            .,
            "${pheno}-overlap.sumstat.gz",
            quote = F,
            na = "NA",
            sep = "\\t"
        )
    write.table(overlap, "${pheno}-overlap.snp", quote=F, row.names=F, col.names=F)
    """
}

process perform_gwas{
    label 'normal'
    afterScript 'rm *qassoc'
    input: 
        tuple   val(name),
                val(phenoName),
                path(pheno),
                path(snp),
                path(bed),
                path(bim), 
                path(fam)
    output:
        tuple   val(name),
                path("${name}.assoc.gz"),
                val("SNP"),
                val("A1"),
                val("A2"),
                val("BETA"),
                val("P"),
                val("SE"),
                val("NMISS"),
                val("null"),
                val("true")
    script:
    base=bed.baseName
    """
    plink \
        --bfile ${base} \
        --extract ${snp} \
        --assoc \
        --pheno ${pheno} \
        --pheno-name ${phenoName} \
        --out ${name} \
        --autosome \
        --allow-no-sex
    awk 'NR==FNR{a[\$2]=\$5; b[\$2]=\$6} NR!=FNR && FNR==1{print \$0,"A1 A2"} NR != FNR && FNR!=1 {print \$0,a[\$2],b[\$2]}' ${bim} ${name}.qassoc | gzip > ${name}.assoc.gz
    """
}


process meta_analysis{
    label 'medium'
    input:
        tuple   val(pheno),
                path(target),
                path(base)
    output:
        tuple   val(pheno),
                path(base),
                path(target),
                path("${pheno}-meta1.assoc.gz")
    script:
    """
    echo "
    SCHEME SAMPLESIZE
    MARKER SNP
    ALLELE A1 A2
    EFFECT BETA
    PVALUE P
    WEIGHT N
    PROCESS ${base}
    
    MARKER SNP
    ALLELE A1 A2
    EFFECT BETA
    PVALUE P
    WEIGHT N
    PROCESS ${target}

    OUTFILE ${pheno}-meta .assoc
    ANALYZE 
    " > metal_command
    metal < metal_command
    gzip ${pheno}-meta1.assoc
    """
}

                  
process modify_metal{
    label 'medium'
    input:
        tuple   val(pheno),
                path(base),
                path(target),
                path(meta)
    output:
        tuple   val(pheno),
                path("${pheno}-meta.assoc.gz")
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    sumstat <- fread("${meta}")
    sumstat[, N := 0]
    setnames(sumstat, "MarkerName", "SNP")
    setnames(sumstat, "Allele1", "A1")
    setnames(sumstat, "Allele2", "A2")
    setnames(sumstat, "P-value", "P")
    add_n <- function(file, input, name){
        dat <- fread(file) %>%
            setnames(., name, "N2")
        input <- merge(input, dat[,c("SNP", "N2")], by = "SNP") %>%
            .[, N := N + N2] %>%
            .[, -c("N2")]
    }
    # This should work because data.table are passed by reference (no copy)
    dat <- fread("${base}")
    if("NMISS" %in% colnames(dat)){
        setnames(dat, "NMISS", "N2")
    }else{
        setnames(dat, "N", "N2")
    }
        
    sumstat <- merge(sumstat, dat[,c("SNP", "N2")], by = "SNP") %>%
            .[, N := N + N2] %>%
            .[, -c("N2")]
    dat <- fread("${target}")
    if("NMISS" %in% colnames(dat)){
        setnames(dat, "NMISS", "N2")
    }else{
        setnames(dat, "N", "N2")
    }
    sumstat <- merge(sumstat, dat[,c("SNP", "N2")], by = "SNP") %>%
            .[, N := N + N2] %>%
            .[, -c("N2")]
    sumstat <- sumstat[,-c("Weight")]
    setnames(sumstat, "Zscore", "BETA")
    fwrite(
      sumstat,
      "${pheno}-meta.assoc.gz",
      sep = "\\t",
      na = "NA",
      quote = F
    )
    """
}



process filter_fam{
    label 'normal'
    input:
        tuple   path(bed),
                path(bim),
                path(fam),
                path(qcFam),
                path(dropout)
    output:
        path("valid.fam")
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    drop <- fread("${dropout}", header = F)
    qc <- fread("${qcFam}", header = F)
    fread("${fam}", header = F) %>%
        .[V2 %in% qc[,V2]] %>%
        .[! V2 %in% drop[,V1]] %>%
        fwrite(., "valid.fam", sep = "\\t", quote = F, na = "NA")
    """
}