process get_overlap_snps{
    executor 'local'
    label 'tiny'
    // This process extract SNPs found in both the genotype and the QCed SNPs.
    // Reformat the output to match the required input format of MAGMA
    input: 
        tuple   val(pheno),
                path(snp),  // contain overlapped SNPs between GWAS and bim
                path(bed), 
                path(bim), 
                path(fam)
    output:
        tuple   val(pheno),
                path ("${pheno}-extracted")
    script: 
    """
        awk 'NR==FNR {a[\$2]=2} NR!=FNR && \$2 in a {print \$2,\$1,\$4}' ${snp} ${bim} > ${pheno}-extracted
    """
}

process perform_annotation_for_simulated{
    label 'tiny'
    // this process generate the required annotation
    input:
        tuple   path(bed),
                path(bim),
                path(fam),
                path(geneLoc),
                val(wind5),
                val(wind3)
    output:
        path("magma.genes.annot")

    script:    
    """
    wind_com=""
    if [ ! -z "${wind5}" ]; then
        wind_com="window=${wind5},${wind3}"
        if [ ${wind5} -eq ${wind3} ] || [ -z "${wind3}" ]; then
            wind_com="window=${wind5}"
        fi
    fi
    awk '{print \$2,\$1,\$4}' ${bim} > snp
    ./${magma} \
        --annotate \${wind_com}\
        --snp-loc snp \
        --gene-loc ${geneLoc} \
        --out magma
    """
}

process perform_annotation{
    label 'tiny'
    input:
        tuple   val(pheno),
                path(snp),
                path(geneLoc),
                val(wind5),
                val(wind3)
    output:
        tuple   val(pheno),
                path("${pheno}.genes.annot")

    script:    
    """
    wind_com=""
    if [ ! -z "${wind5}" ]; then
        wind_com="window=${wind5},${wind3}"
        if [ ${wind5} -eq ${wind3} ] || [ -z "${wind3}" ]; then
            wind_com="window=${wind5}"
        fi
    fi
    magma \
        --annotate \${wind_com}\
        --snp-loc ${snp} \
        --gene-loc ${geneLoc} \
        --out ${pheno}
    """
}

process magma_simulated_genotype{
    module "cmake"
    queue 'express'
    time '12h'
    memory '10G'
    input:
        tuple   val(perm),
                val(numSet),
                val(herit),
                val(size),
                path(pheno),
                path(annot),
                path(bed), 
                path(bim), 
                path(fam),
                path(magma)
    output:
        tuple   val(perm),
                val(numSet),
                val(herit),
                val(size),
                path("${perm}-${numSet}-${herit}-${size}.genes.out"),
                path("${perm}-${numSet}-${herit}-${size}.genes.raw")
    script:
    base=bed.baseName
    """
    ./${magma} \
        --bfile ${base} \
        --gene-annot ${annot} \
        --out ${perm}-${numSet}-${herit}-${size} \
        --pheno file=${pheno} use=Pheno
    """
}

process magma_genotype{
    // Run MAGMA on genotyped data. Require the annotation
    // file, genotype file and also the phenotype
    label 'long'
    input:
        tuple   val(tag),
                val(phenoName),
                path(pheno),
                path(annot),
                path(bed), 
                path(bim), 
                path(fam)
    output:
        tuple   val(tag),
                path("${tag}.genes.out"),
                path("${tag}.genes.raw")
    script:
    base=bed.baseName
    """
    magma \
        --bfile ${base} \
        --gene-annot ${annot} \
        --out ${tag} \
        --pheno file=${pheno} use=${phenoName}
    """
}

process magma_simulated_sumstat{
    module "cmake"
    afterScript "rm ${pheno}.sumstat"
    time '12h'
    memory '20G'
    queue 'express'

    input:
        tuple   val(perm),
                val(numSet),
                val(herit),
                path(ss),
                path(annot),
                path(bed),
                path(bim),
                path(fam),
                path(magma)
    output:
        tuple   val(perm),
                val(numSet),
                val(herit),
                path("${perm}-${numSet}-${herit}-sumstat.genes.out"), 
                path("${perm}-${numSet}-${herit}-sumstat.genes.raw")
    script:
    base=bed.baseName
    """
    if gzip -t ${ss}; then
        zcat ${ss} > ${perm}-${numSet}-${herit}-sumstat
    else
        cat ${ss} > ${perm}-${numSet}-${herit}-sumstat
    fi
    ./${magma} \
        --bfile ${base} \
        --gene-annot ${annot}  \
        --pval ${perm}-${numSet}-${herit}-sumstat use=SNP,P ncol=NMISS \
        --out ${perm}-${numSet}-${herit}-sumstat
    """
}

process magma_sumstat{
    // Run MAGMA on the summary statistic file. 
    // The genotype file is also included for LD
    // calculation
    afterScript "rm ${pheno}.sumstat"
    label 'long'

    input:
        tuple   val(pheno),
                path(ss),
                path(annot),
                path(bed),
                path(bim),
                path(fam)
    output:
        tuple   val(pheno),
                path("${pheno}.sumstat.genes.out"), 
                path("${pheno}.sumstat.genes.raw")
    script:
    base=bed.baseName
    """
    # MAGMA doesn't support GZ input. Therefore we gunzip
    # the summary statistic file if it is gzipped
    if gzip -t ${ss}; then
        zcat ${ss} > ${pheno}.sumstat
    else
        cat ${ss} > ${pheno}.sumstat
    fi
    magma \
        --bfile ${base} \
        --gene-annot ${annot}  \
        --pval ${pheno}.sumstat use=SNP,P ncol=N \
        --out ${pheno}.sumstat
    """
}

process magma_meta{
    // Run the MAGMA meta-analysis, combining results 
    // from the genotyped and summary stat MAGMA run
    label 'long'
    input:
        tuple   val(pheno),
                path(ss_out), 
                path(ss_raw),
                path(raw_out),
                path(raw_raw)
    output:
        tuple   val(pheno),
                path ("${pheno}.meta.genes.raw")
    script:
    """
    magma \
        --meta raw=${raw_raw},${ss_raw}  \
        --out ${pheno}.meta
    """
}


process magma_gene_set{
    // Run the MAGMA set based analysis
    label 'long'
    input:
        tuple   val(pheno),
                path(raw),
                path(gmt)
    output:
        tuple   val(pheno),
                val("Set"),
                path("${pheno}.gsa.out")
    script:
    """
    magma \
        --gene-results ${raw} \
        --set-annot ${gmt} \
        --out ${pheno}
    """
}

process magma_specificity{
    // Try to run MAGMA's celltype specificity method just for completeness. 
    // We don't look at this result in the paper, but it is something good to have 
    label 'long'
    input:
        tuple   path(spec),
                val(pheno),
                path(raw)
    output:
        tuple   val(pheno),
                val("Specificity"),
                path("${pheno}.${base}.celltype.gsa.out") 
    script:
    base=spec.baseName
    """
    magma \
        --gene-results ${raw} \
        --gene-covar ${spec} \
        --model direction-covar=greater \
        --out ${pheno}.${base}.celltype
    """
}

process modify_simulated_magma_output{
    label 'normal'
    input:
        tuple   val(perm),
                val(numSet),
                val(herit),
                val(targetSize),
                val(baseSize),
                path(gsa)
    output:
        tuple   val(perm),
                val(numSet),
                val(herit),
                val(targetSize),
                val(baseSize),
                path("${perm}-${numSet}-${herit}-${targetSize}-${baseSize}-magma.csv")
    script:
    """
    #!/usr/bin/env Rscript
    library(data.table)
    library(magrittr)
    result <- fread(cmd = paste0("grep -v ^# ${gsa}")) %>%
        setnames(. , "FULL_NAME", "Set") %>%
        setnames(., "BETA", "Coefficient") %>%
        .[, c("Set", "Coefficient", "P")] %>%
        .[, Software := "MAGMA"] %>%
        .[, Perm := "${perm}"] %>%
        .[, NumSet := "${numSet}"] %>%
        .[, Heritability := "${herit}"] %>%
        .[, TargetSize := "${targetSize}"] %>%
        .[, BaseSize := "${baseSize}"] %>%
        fwrite(., "${perm}-${numSet}-${herit}-${targetSize}-${baseSize}-magma.csv")
    """
}
process modify_magma_output{
    module 'R/4.0.3'
    cpus '1'
    memory '10G'
    time '1h'
    queue 'express'
    input:
        tuple   val(pheno),
                val(type),
                path("*")
    output:
        tuple   val(pheno),
                val(type),
                path("${pheno}-${type}-magma.csv")
    script:
    """
    #!/usr/bin/env Rscript
    library(data.table)
    library(magrittr)
    files <- list.files()
    result <- NULL
    for(i in files){
        tmp <- fread(cmd = paste0("grep -v ^# ",i)) %>%
            setnames(. , "FULL_NAME", "Set") %>%
            setnames(., "BETA", "Coefficient") %>%
            .[, c("Set", "Coefficient", "P")]
        result %<>% rbind(., tmp)
    }
    result %>%
        .[, Software := "MAGMA"] %>%
        .[, Type := "${type}"] %>%
        .[, Trait := "${pheno}"] %>%
        fwrite(., "${pheno}-${type}-magma.csv")
    """
}
