
process supervised_pathway_classification{
    
    label 'set_glmnet'
    input:
        tuple   val(traitA),
                val(traitB),
                val(fold),
                val(type),
                val(sensitivity),
                path(training),
                path(validate),
                val(gtf),
                val(software),
                path(summary),
                path(best),
                path(snps)
    output:
        path "${traitA}-${traitB}-${type}-${fold}-${sensitivity}-${gtf}-${software}.csv", emit: res
        path "${traitA}-${traitB}-${type}-${fold}-${sensitivity}-${gtf}-${software}.set", emit: set
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(glmnet)
    library(data.table)
    library(doParallel)
    registerDoParallel(${task.cpus})

    performance <- fread("${summary}") %>%
      .[Set != "Base"]
    # Filter by Competitive P only when it is available
    if("Competitive.P" %in% colnames(performance)){
        performance %<>%
            .[, Competitive.P := as.numeric(Competitive.P)] %>%
            .[!is.na(Competitive.P) & Competitive.P < 0.05]
    }
    select <- c("FID", "IID", performance[, Set])
    prs <- fread("${best}", select = select)
    # Remove duplicated PRS. This is not required as in theory, glmnet should 
    # double weight the duplicates. However, this should help reduce the size 
    # of the matrix, therefore improve our performance
    dup.col <- !duplicated(as.list(prs))
    # Add back IID, which is usually duplicate of FID
    dup.col[2] <- T
    select <- colnames(prs)[dup.col]
    select <- select[-c(1:2)]
    prs <- prs[,dup.col, with=F]
    training.pheno <- fread("${training}") %>%
        .[, c("FID", "IID", "PhenoAdj")] %>%
        .[!is.na(PhenoAdj)]
    valid.pheno <- fread("${validate}") %>%
        .[, c("FID", "IID", "PhenoAdj")] %>%
        .[!is.na(PhenoAdj)]
    
    prs.target <- prs %>%
        merge(training.pheno[,c("FID", "IID", "PhenoAdj")], .)
    snps <- fread("${snps}")
    num.genome <- sum(snps[,Base])
    included <- snps[,select, with=F] %>%
        apply(., 1, sum) 
    num.snps <- sum(included > 0)
    get.prs.r2 <- function(model, input, mat){
      pred <- predict(model, newx=mat, type="response", s="lambda.min")
      obs.r2 <- cor(pred, input[,PhenoAdj])^2
      return(obs.r2)
    }
    x.matrix <- model.matrix(~., prs.target[, -c("FID", "IID", "PhenoAdj")])[,-1]
    model <-
      cv.glmnet(y = as.matrix(prs.target[, c("PhenoAdj")]), 
                x = x.matrix,
                parallel=T)
    coefs <- coef.glmnet(model, s="lambda.min")
    coef.res <-  data.frame(Set=row.names(coefs)[coefs[,1] != 0], Coef=coefs[coefs[,1] != 0]) %>%
        as.data.table %>%
        .[, Sample.Type := "${type}"] %>%
        .[, Analysis.Type := "${sensitivity}"] %>%
        .[, GTF.Type := "${gtf}"] %>%
        .[, Fold := "${fold}"] %>%
        .[, TraitA := "${traitA}"] %>%
        .[, TraitB := "${traitB}"] %>%
        .[, Software := "${software}"]
    fwrite(coef.res, "${traitA}-${traitB}-${type}-${fold}-${sensitivity}-${gtf}-${software}.set")
    prs.validate <- prs %>%
      merge(valid.pheno[,c("FID", "IID", "PhenoAdj")], .)
    # Can get R2 by doing the following
    validate.matrix <- model.matrix(~., prs.validate[, -c("FID", "IID", "PhenoAdj")])[,-1]
    training.r2 <- get.prs.r2(model, prs.target, x.matrix)
    validate.r2 <- get.prs.r2(model, prs.validate, validate.matrix)
    #validate.refit <- refit.r2(model, prs.validate, validate.matrix)
    res <- data.table(
      Sample = "Training",
      R2 = training.r2[1]
    ) %>%
      rbind(
        .,
        data.table(
          Sample = "Validate",
          R2 = validate.r2[1]
        )
      ) %>%
      .[, Sample.Type := "${type}"] %>%
      .[, Analysis.Type := "${sensitivity}"] %>%
      .[, GTF.Type := "${gtf}"] %>%
      .[, Fold := "${fold}"] %>%
      .[, TraitA := "${traitA}"] %>%
      .[, TraitB := "${traitB}"] %>%
      .[, Software := "${software}"] %>%
      .[, Genome := num.genome ] %>%
      .[, Num.SNP := num.snps]
    colnames(res) <- gsub(".V1", "", colnames(res))
    fwrite(res, "${traitA}-${traitB}-${type}-${fold}-${sensitivity}-${gtf}-${software}.csv")
    """
}

process supervised_classification_with_single_prs{
    label 'set_glmnet'
    input:
        tuple   val(traitA),
                val(traitB),
                val(fold),
                val(type),
                val(sensitivity),
                path(training),
                path(validate),
                val(software),
                path(best),
                path(allscore)
    output:
        path "${traitA}-${traitB}-${type}-${fold}-${sensitivity}-${software}.csv"

    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    library(glmnet)
    library(doParallel)
    registerDoParallel(${task.cpus})
    # Mainly aggregate result and calculate MSE
    prs <- fread("${best}", select = c("FID", "IID", "PRS"))
    training.pheno <- fread("${training}") %>%
        .[, c("FID", "IID", "PhenoAdj")] %>%
        .[!is.na(PhenoAdj)]
    valid.pheno <- fread("${validate}") %>%
        .[, c("FID", "IID", "PhenoAdj")] %>%
        .[!is.na(PhenoAdj)]
    prs.training <- merge(training.pheno, prs)
    prs.validate <- merge(valid.pheno, prs)
    get.prs.r2 <- function(x, obs.model) {
        obs.r2 <- predict(obs.model, newdata=x, type="response") %>%
            cor(., x[,PhenoAdj])
        obs.r2 <- obs.r2^2
        return(obs.r2)
    }
    obs.model <- 
            glm("PhenoAdj~PRS", data = prs.training) 
    training.r2 <- get.prs.r2(prs.training, obs.model)
    validate.r2 <- get.prs.r2(prs.validate, obs.model)

    # Then we do the same analysis but with all scores
    # Format is always FID IID PRS1 PRS2 etc
    all <- fread("${allscore}")
    # This is for lassosum as there can be multiple column with 0
    dup.col <- !duplicated(as.list(all))
    dup.col[2] <- T
    all <- all[,dup.col, with=F]
    colnames(all) <- c("FID", "IID", paste("V",1:(ncol(all)-2), sep=""))
    prs.training <- merge(training.pheno, all)
    prs.validate <- merge(valid.pheno, all)
    # Now do glm model
    get.glmnet.r2 <- function(model, input, mat){
      pred <- predict(model, newx=mat, type="response", s="lambda.min")
      obs.r2 <- cor(pred, input[,PhenoAdj])^2
      return(obs.r2)
    }
    x.matrix <- model.matrix(~., prs.training[, -c("FID", "IID", "PhenoAdj")])[,-1]
    model <-
      cv.glmnet(y = as.matrix(prs.training[, c("PhenoAdj")]), 
                x = x.matrix,
                parallel=T)
    validate.matrix <- model.matrix(~., prs.validate[, -c("FID", "IID", "PhenoAdj")])[,-1]
    training.glmnet.r2 <- get.glmnet.r2(model, prs.training, x.matrix)
    validate.glmnet.r2 <- get.glmnet.r2(model, prs.validate, validate.matrix)
    res <- data.table(Sample = "Training",
               R2 = training.r2[1],
               Software = "${software}") %>%
        rbind(.,
              data.table(Sample = "Validate",
                         R2 = validate.r2[1],
               Software = "${software}")) %>%
        rbind(.,
              data.table(Sample = "Training",
                         R2 = training.glmnet.r2[1],
               Software = "${software}GLM")) %>%
        rbind(.,
              data.table(Sample = "Validate",
                         R2 = validate.glmnet.r2[1],
               Software = "${software}GLM")) %>%
        .[, Sample.Type := "${type}"] %>%
        .[, Fold := "${fold}"] %>%
        .[, Analysis.Type := "${sensitivity}"] %>%
        .[, GTF.Type := NA] %>%
        .[, TraitA := "${traitA}"] %>%
        .[, TraitB := "${traitB}"] 
    colnames(res) <- gsub(".V1", "", colnames(res))
    fwrite(res, "${traitA}-${traitB}-${type}-${fold}-${sensitivity}-${software}.csv")
    """
}

process prediction_with_pathway{
    queue 'express'
    time '12h'
    module 'R/4.0.3'
    //executor 'local'
    cpus '48'
    maxForks '20'
    //memory '80G'
    //errorStrategy 'ignore'
    clusterOptions = '-P acc_psychgen -M 41440 -R select[mem>=1280] rusage[mem=1280]'
    input:
        tuple   val(name),
                val(fold),
                val(group),
                val(software),
                path(training),
                path(validate),
                path(summary),
                path(best),
                path(snps)
     output:
        path "${name}-${fold}-${group}-${software}.csv", emit: res
        path "${name}-${fold}-${group}-${software}.set", emit: set
    
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(glmnet)
    library(data.table)
    library(doParallel)
    #registerDoParallel(48)
    registerDoParallel(${task.cpus})
    performance <- fread("${summary}") %>%
      .[Set != "Base"] %>%
      .[, Competitive.P := as.numeric(Competitive.P)] %>%
      .[!is.na(Competitive.P) & Competitive.P < 0.05] %>%
      .[!grepl("_NULL", Set)]
    select <- c("FID", "IID", performance[, Set])
    training.pheno <- fread("${training}") %>%
        .[, c("FID", "IID", "PhenoAdj")]
    valid.pheno <- fread("${validate}") %>%
        .[, c("FID", "IID", "PhenoAdj")]
    prs <- fread("${best}", select = select)
    prs.target <- prs %>%
        merge(training.pheno, .) %>%
        .[, c(
            "FID",
            "IID",
            "PhenoAdj",
            performance[, Set]
        ), with = F]
    get.prs.r2 <- function(model, input, mat){
      pred <- predict(model, newx=mat, type="response", s="lambda.min")
      obs.r2 <- cor(pred, input[,PhenoAdj])^2
      return(obs.r2)
    }
    x.matrix <- model.matrix(~., prs.target[, -c("FID", "IID", "PhenoAdj")])[,-1]
    model <-
      cv.glmnet(y = as.matrix(prs.target[, c("PhenoAdj")]), 
                x = x.matrix,
                parallel=T)
    coefs <- coef.glmnet(model, s="lambda.min")
    coef.res <-  data.frame(Set=row.names(coefs)[coefs[,1] != 0], Coef=coefs[coefs[,1] != 0]) %>%
        as.data.table %>%
        .[, GTF.Type := "${group}"] %>%
        .[, Fold := "${fold}"] %>%
        .[, Trait := "${name}"] %>%
        .[, Software := "${software}"]
    fwrite(coef.res, "${name}-${fold}-${group}-${software}.set")
    prs.validate <- prs %>%
      merge(valid.pheno, .) %>%
        .[, c(
            "FID",
            "IID",
            "PhenoAdj",
            performance[, Set]
        ), with = F]
    # Can get R2 by doing the following
    validate.matrix <- model.matrix(~., prs.validate[, -c("FID", "IID", "PhenoAdj")])[,-1]
    training.r2 <- get.prs.r2(model, prs.target, x.matrix)
    validate.r2 <- get.prs.r2(model, prs.validate, validate.matrix)
    #validate.refit <- refit.r2(model, prs.validate, validate.matrix)
    snps <- fread("${snps}")
    genome <- sum(snps[,Base])
    included <- snps[,-c("CHR","SNP","BP","P","Base","Background")] %>%
        apply(., 1, sum) 
    num.snps <- sum(included > 0)
    res <- data.table(
      Sample = "Training",
      R2 = training.r2[1]
    ) %>%
      rbind(
        .,
        data.table(
          Sample = "Validate",
          R2 = validate.r2[1]
        )
      ) %>%
      .[, GTF.Type := "${group}"] %>%
      .[, Fold := "${fold}"] %>%
      .[, Trait := "${name}"] %>%
      .[, Software := "${software}"] %>%
      .[, Num.SNP := num.snps] %>%
      .[, GW.SNP := genome] 

    colnames(res) <- gsub(".V1", "", colnames(res))
    fwrite(res, "${name}-${fold}-${group}-${software}.csv")
    """
}
process modify_classification_results{
    module 'R/4.0.3'
    queue 'express'
    time '1h'
    memory '5G'
    input:
        path(input)
    output:
        path("${input}.mod")
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    fread("${input}") %>%
        .[, -c("Sample.Type", "Analysis.Type", "TraitB")] %>%
        setnames(., "TraitA", "Trait") %>%
        fwrite("${input}.mod")
    """
}



process ibd_single_classification{
    module 'R/4.0.3'
    queue 'express'
    time '2h'
    cpus '48'
    //memory '80G'
    //errorStrategy 'ignore'
    clusterOptions = '-P acc_psychgen -M 41440 -R select[mem>=1280] rusage[mem=1280]'
    input:
        tuple   val(name),
                val(gwasPheno),
                val(fold),
                val(type),
                path(summary),
                path(best),
                path(training),
                path(validate)
    output:
        path "${name}-${gwasPheno}-${fold}-${type}-single"
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    library(cluster)
    library(pdfCluster)
    library(glmnet)
    library(doParallel)
    registerDoParallel(${task.cpus})
    best <- fread("${best}")
    
    best[, PRS := lapply(.SD, scale), .SDcols=c("PRS")]
    training <- fread("${training}")
    training.prs <- merge(training, best[,-c("In_Regression")])
    # -1 to remove intercept
    print("start performing glm")
    get.prs.r2 <- function(x, obs.model) {
        obs.r2 <- predict(obs.model, newdata=x, type="response") %>%
            cor(., x[,PhenoAdj])
        obs.r2 <- obs.r2^2
        return(obs.r2)
    }
    obs.model <- 
            glm("PhenoAdj~PRS", data = training.prs) 
    training.r2 <- get.prs.r2(training.prs, obs.model)
    NagelkerkeR2 <- function (rr){
        n <- nrow(rr\$model)
        R2 <- (1 - exp((rr\$dev - rr\$null)/n))/(1 - exp(-rr\$null/n))
        return(R2)
    }
    validate <- fread("${validate}")
    validate.prs <- merge(validate, best[,c("FID", "IID", "PRS"), with=F])
    if("${type}" == "Unsupervised"){
        print("start performing k mean clustering")
        dist.matrix <- dist(validate.prs[,"PRS", with=F])
        k.res <- NULL
        for(i in 1:30){
            km.multi <- kmeans( validate.prs[,"PRS", with=F],
                                centers=i,
                                algorithm="MacQueen",
                                iter.max=1000)
            multi.ss <- silhouette(km.multi\$cluster, dist.matrix)
            if(i == 1){
                multi.ss <- as.matrix(data.table(0,0,0))
            }
            k.res %<>% rbind(.,
                data.table( k=i,
                            ss=mean(multi.ss[,3])))
        }
        best.k <- k.res[which.max(ss), k]
        cluster <- kmeans(validate.prs[, "PRS", with=F], 
                        centers=best.k,
                        algorithm = "MacQueen",
                        iter.max=1000)
        validate.prs[, Group := cluster\$cluster]
        prediction <- glm(subtype~Group, validate.prs, family=binomial)
        r2 <- NagelkerkeR2(prediction)


        cluster <- kmeans(validate.prs[, "PRS", with=F], 
                        centers=2,
                        algorithm = "MacQueen",
                        iter.max=1000)
        validate.prs[, Group := cluster\$cluster]
        prediction.k2 <- glm(subtype~Group, validate.prs, family=binomial)
        r2.k2 <- NagelkerkeR2(prediction.k2)
        data.table( Fold = ${fold}, 
                    Trait = "${name}",
                    GWAS = "${gwasPheno}",
                    Type = "${type}",
                    Software = "PRSice",
                    Best.K = best.k, 
                    Validate.R2 = r2,
                    Training.R2 = training.r2,
                    Coefficient = coef(summary(prediction))[2,1],
                    SE = coef(summary(prediction))[2,2],
                    P = coef(summary(prediction))[2,4],
                    K2.R2 = r2.k2,
                    K2.Coefficient = coef(summary(prediction.k2))[2,1],
                    K2.SE = coef(summary(prediction.k2))[2,2],
                    K2.P = coef(summary(prediction.k2))[2,4]) %>%
            fwrite(., "${name}-${gwasPheno}-${fold}-${type}-single")
    }else{
        pred <- predict(obs.model, newdata=validate.prs, type="response")
        validate.prs[,Pred := pred]
        prediction <- glm(subtype~Pred, validate.prs, family=binomial)
        r2 <- NagelkerkeR2(prediction)
        data.table( Fold = ${fold}, 
                    Trait = "${name}",
                    GWAS = "${gwasPheno}",
                    Type = "${type}",
                    Software = "PRSice",
                    Best.K = NA, 
                    Validate.R2 = r2,
                    Training.R2 = training.r2,
                    Coefficient = coef(summary(prediction))[2,1],
                    SE = coef(summary(prediction))[2,2],
                    P = coef(summary(prediction))[2,4],
                    K2.R2 = NA,
                    K2.Coefficient = NA,
                    K2.SE = NA,
                    K2.P = NA) %>%
            fwrite(., "${name}-${gwasPheno}-${fold}-${type}-single")
    }
    """
}
process ibd_subtype_classification{
    module 'R/4.0.3'
    queue 'express'
    time '2h'
    cpus '48'
    //memory '80G'
    //errorStrategy 'ignore'
    clusterOptions = '-P acc_psychgen -M 41440 -R select[mem>=1280] rusage[mem=1280]'
    input:
        tuple   val(name),
                val(gwasPheno),
                val(fold),
                val(type),
                path(training),
                path(validate),
                path(best)
    output:
        path "${name}-${gwasPheno}-${fold}-${type}"
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    library(cluster)
    library(pdfCluster)
    library(glmnet)
    library(doParallel)
    registerDoParallel(${task.cpus})
    best <- fread("${best}")
    dup <-  !duplicated(as.list(best))
    dup[2] <- T # Set IID as True
    scales  <- colnames(best[, -c("FID", "IID", "In_Regression", "Base")])
    best[, (scales) := lapply(.SD, scale), .SDcols=scales]
    training <- fread("${training}")
    training.prs <- merge(training, best[,dup, with=F][,-c("In_Regression", "Base")])
    # -1 to remove intercept
    print("start performing glmnet")
    x.matrix <- model.matrix(~., training.prs[, -c("FID", "IID", "PhenoAdj")])[,-1]
    model <- cv.glmnet(y = as.matrix(training.prs[, c("PhenoAdj")]), x = x.matrix,
                parallel=T)
    coefs <- coef(model, s='lambda.min')
    selected <- gsub("`","",rownames(coefs)[coefs[,1] != 0])
    selected  <- selected[-1]
    print("pathway selected")
    validate <- fread("${validate}")
    validate.prs <- merge(validate, best[,c("FID", "IID", selected), with=F])
    get.glmnet.r2 <- function(model, input, mat){
      pred <- predict(model, newx=mat, type="response", s="lambda.min")
      obs.r2 <- cor(pred, input[,PhenoAdj])^2
      return(obs.r2)
    }
    
    training.glmnet.r2 <- get.glmnet.r2(model, training.prs, x.matrix)

    NagelkerkeR2 <- function (rr){
        n <- nrow(rr\$model)
        R2 <- (1 - exp((rr\$dev - rr\$null)/n))/(1 - exp(-rr\$null/n))
        return(R2)
    }
    if("${type}" == "Unsupervised"){
        print("start performing k mean clustering")
        dist.matrix <- dist(validate.prs[,selected, with=F])
        k.res <- NULL
        for(i in 1:30){
            km.multi <- kmeans( validate.prs[,selected, with=F],
                                centers=i,
                                algorithm="MacQueen",
                                iter.max=1000)
            multi.ss <- silhouette(km.multi\$cluster, dist.matrix)
            if(i == 1){
                multi.ss <- as.matrix(data.table(0,0,0))
            }
            k.res %<>% rbind(.,
                data.table( k=i,
                            ss=mean(multi.ss[,3])))
        }
        best.k <- k.res[which.max(ss), k]
        cluster <- kmeans(validate.prs[, selected, with=F], 
                        centers=best.k,
                        algorithm = "MacQueen",
                        iter.max=1000)
        validate.prs[, Group := cluster\$cluster]
        prediction <- glm(subtype~Group, validate.prs, family=binomial)
        r2 <- NagelkerkeR2(prediction)

        cluster <- kmeans(validate.prs[, selected, with=F], 
                        centers=2,
                        algorithm = "MacQueen",
                        iter.max=1000)
        validate.prs[, Group := cluster\$cluster]
        prediction.k2 <- glm(subtype~Group, validate.prs, family=binomial)
        r2.k2 <- NagelkerkeR2(prediction.k2)
        data.table( Fold = ${fold}, 
                    Trait = "${name}",
                    GWAS = "${gwasPheno}",
                    Type = "${type}",
                    Software = "PRSet",
                    Best.K = best.k, 
                    Validate.R2 = r2,
                    Training.R2 = training.glmnet.r2,
                    Coefficient = coef(summary(prediction))[2,1],
                    SE = coef(summary(prediction))[2,2],
                    P = coef(summary(prediction))[2,4],
                    K2.R2 = r2.k2,
                    K2.Coefficient = coef(summary(prediction.k2))[2,1],
                    K2.SE = coef(summary(prediction.k2))[2,2],
                    K2.P = coef(summary(prediction.k2))[2,4]) %>%
            fwrite(., "${name}-${gwasPheno}-${fold}-${type}")
    }else{
        validate.prs <- merge(validate, best[,c("FID", "IID", gsub("`","",colnames(x.matrix))), with=F])
        validate.matrix <- model.matrix(~., validate.prs[ ,gsub("`","",colnames(x.matrix)), with=F])[,-1]
        pred <- predict(model, newx=validate.matrix, type="response", s="lambda.min")
        validate.prs[,Pred := pred]
        prediction <- glm(subtype~Pred, validate.prs, family=binomial)
        r2 <- NagelkerkeR2(prediction)
        data.table( Fold = ${fold}, 
                    Trait = "${name}",
                    GWAS = "${gwasPheno}",
                    Type = "${type}",
                    Software = "PRSet",
                    Best.K = NA, 
                    Validate.R2 = r2,
                    Training.R2 = training.glmnet.r2,
                    Coefficient = coef(summary(prediction))[2,1],
                    SE = coef(summary(prediction))[2,2],
                    P = coef(summary(prediction))[2,4],
                    K2.R2 = NA,
                    K2.Coefficient = NA,
                    K2.SE = NA,
                    K2.P = NA) %>%
            fwrite(., "${name}-${gwasPheno}-${fold}-${type}")
    }
    """
}


