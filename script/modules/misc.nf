process combine_gmt{
    executor 'local'
    label 'tiny'
    input:
        path(biochem)
        path(specificity)
    output:
        path("GeneSet.gmt")
    script:
    """
        cat ${biochem} ${specificity} > result
        mv result GeneSet.gmt
    """
}

process mock_file{
    label 'tiny'
    executor 'local'
    input:
        val(input)
    output:
        path("${input}")
    script:
    """
    touch ${input}
    """
}
process group_bim{
    label 'tiny'
    executor 'local'
    input:
        path("*")
    output:
        tuple   path("combined.bed"),
                path("combined.bim"),
                path("combined.fam")
    script:
    """
    cat *bim > combined.bim
    touch combined.bed
    touch combined.fam
    """
}

process mock_qc{
    label 'tiny'
    executor 'local'
    input:
        tuple   path(bed),
                path(bim),
                path(fam)
    output:
        path "mock_qc"
    script:
    """
    awk 'NR==1{print "SNP"} {print \$2}' ${bim} > mock_qc
    """
}
process combine_files{
    executor 'local'
    label 'normal'
    publishDir "${dir}", mode: 'copy'
    input:
        val(name)
        val(dir)
        path("*")
    output:
        path("${name}")
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    files <- list.files()
    res <- NULL
    for(i in files){
        res %<>% rbind(., fread(i), fill=T)
    }
    fwrite(res, "${name}", sep="\\t")
    """
}

process filter_gmt{
    executor 'local'
    memory '10G'
    time '1h'
    input:
        path("*")
    output:
        path("GeneSet.gmt")
    script:
    """
    cat *.gmt >> result
    grep -v _NULL result > GeneSet.gmt
    """
}

process gene_shift_gtf{
    label 'normal'
    input:
        path(gtf)
        val(extension)
    output:
        tuple   val("Null"),
                path("null.gtf.gz")
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table, )
    fread("${gtf}") %>%
      .[V2 == "protein_coding" & V3 == "gene"] %>%
      .[, V4 := V4+ ${extension}] %>%
      .[, V5 := V5+ ${extension}] %>%
      unique %>%
      fwrite(., "null.gtf.gz", sep="\\t", na="NA", quote=F)
    """
}