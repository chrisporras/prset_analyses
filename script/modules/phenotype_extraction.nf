process split_samples_for_cross_validation{
    label 'tiny'
    input:
        tuple   val(traitA),
                val(traitB),
                val(type),
                val(sensitivity),
                path(pheno),
                val(fold)
    output:
        tuple   val(traitA),
                val(traitB),
                val(fold),
                val(type),
                val(sensitivity),
                path("${traitA}-${traitB}-${fold}-${type}.training"),
                path("${traitA}-${traitB}-${fold}-${type}.validate")
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    pheno <- fread("${pheno}")
    pheno[Folds == ${fold}] %>%
        fwrite(., "${traitA}-${traitB}-${fold}-${type}.validate", sep="\\t")
    pheno[Folds != ${fold}, PhenoAdj := NA] %>%
        fwrite(., "${traitA}-${traitB}-${fold}-${type}.training", sep="\\t", na="NA", quote=F)
    """
}

process assign_fold_to_composite_trait{
    label 'normal'
    input:
        tuple   val(traitA),
                path(phenoA),
                val(traitB),
                path(phenoB),
                val(maxFold),
                val(type),
                val(sensitivity)
                
    output:
        tuple   val(traitA),
                val(traitB),
                val(type),
                val(sensitivity),
                path("${traitA}-${traitB}-${type}-${sensitivity}.csv"), emit: pheno
        path "${traitA}-${traitB}-${type}-${sensitivity}.info", emit: info
    script:
    // need to be careful with age when merging
    // For now, ignore covariates
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(caret)
    library(data.table)
    phenoA <-
        fread("${phenoA}")[, c(
            "FID",
            "IID",
            "${traitA}",
            "${traitA}Adj",
            paste("PC", 1:15, sep = ""),
            "Age",
            "Sex",
            "Batch",
            "Centre"
        ), with = F]
    phenoB <-
        fread("${phenoB}")[, c("FID", "IID", "${traitB}", "${traitB}Adj", "Age")]
    
    pheno <- merge(phenoA, phenoB, by = c("FID", "IID")) %>%
        .[, Centre := as.factor(Centre)]
    if("${type}" == "Subtype"){
        pheno %<>%
            .[${traitA} > 0] %>%
            .[, Phenotype := ${traitB}]
    } else if ("${type}" == "All") {
        pheno %<>%
            .[, Phenotype :=${traitA}+${traitB}] %>%
            .[Phenotype <= 1, ] %>%
            .[, PhenoAdj := paste("PC", 1:15, sep = "", collapse = "+") %>%
                  paste0("Phenotype~Sex+Age.x+Age.y+Batch+Centre+", .) %>%
                  as.formula %>%
                  glm(., data = .SD, family = binomial) %>%
                  resid]
    } else{
        pheno %<>%
            .[, Phenotype :=${traitA}+${traitB}] %>%
            .[Phenotype > 0 & !Phenotype > 1] %>%
            .[${traitA} == 1, Phenotype := 0]
    }
    pheno[, PhenoAdj := paste("PC", 1:15, sep = "", collapse = "+") %>%
            paste0("Phenotype~Sex+Age.x+Age.y+Batch+Centre+", .) %>%
            as.formula %>%
            glm(., data = .SD, family = binomial) %>%
            resid]
    data.table(
        Case = sum(pheno[, Phenotype] != 0),
        Control = sum(pheno[, Phenotype] == 0),
        Sample.Type = "${type}",
        TraitA = "${traitA}",
        TraitB = "${traitB}",
        Analysis.Type = "${sensitivity}"
    ) %>%
        fwrite(., "${traitA}-${traitB}-${type}-${sensitivity}.info")
    if ("${sensitivity}" == "CV") {
        pheno %>%
            .[, Folds :=  createFolds(paste(${traitA}, ${traitB}, sep = "-"),
            k = ${maxFold},
            list = FALSE)]  %>%
            fwrite(., "${traitA}-${traitB}-${type}-${sensitivity}.csv")
    } else {
        pheno %>%
            .[, Folds :=  createFolds(paste(${traitA}, ${traitB}, sep = "-"),
            k = 2,
            list = FALSE)]  %>%
            fwrite(., "${traitA}-${traitB}-${type}-${sensitivity}.csv")
    }
    """
}

process assign_fold_to_single_trait{
    label 'normal'
    input:
        tuple   val(name),
                path(pheno),
                val(maxFold),
                val(sensitivity)
    output:
        tuple   val(name),
                val(name),
                val("Single"),
                val(sensitivity),
                path("${name}-cv.csv"), emit: pheno
        path "${name}-${sensitivity}-cv.info", emit: info
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(caret)
    library(data.table)
    dat <- fread("${pheno}") %>%
        setnames(., "${name}Adj", "PhenoAdj")
    data.table(
        Case = sum(dat[, ${name}] != 0),
        Control = sum(dat[, ${name}] == 0),
        Sample.Type = "Single",
        TraitA = "${name}",
        TraitB = "${name}",
        Analysis.Type = "${sensitivity}"
    ) %>%
        fwrite(., "${name}-${sensitivity}-cv.info")
    if ("${sensitivity}" == "CV") {
        dat %>%
            .[, Folds :=  createFolds(${name},
            k =${maxFold},
            list = FALSE)]  %>%
            fwrite(., "${name}-cv.csv")
    } else{
        dat %>%
            .[, Folds :=  createFolds(${name},
            k = 2,
            list = FALSE)]  %>%
            fwrite(., "${name}-cv.csv")
    }
    """
}

process dichotomize_phenotype{
    label 'tiny'
    input:
        tuple   val(pheno),
                path(csv)
    output:
        tuple   val(pheno),
                path("${pheno}-binary.csv")
    script:
    """
     #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    pheno <- fread("${csv}")
    if ("${pheno}" == "Height") {
        # Need to generate case control for height
        extreme <-
            pheno[order(-${pheno}), head(.SD, ceiling(.N * 5 / 100)), by = Sex]
        pheno[!is.na(${pheno}), ${pheno} := 0]
        pheno[IID %in% extreme[, IID] & !is.na(${pheno}),${pheno} := 1]
    }else if("${pheno}" == "LDL"){
        # For LDL, we need to handle statin
        cor.factor <- pheno[Statin_Baseline == 0 & Statin_Follow == 1 &
                        !is.na(LDL_Baseline) &
                        !is.na(LDL_Follow), .(Factor = mean(LDL_Follow /
                                                                LDL_Baseline))]
        pheno %<>% .[, LDLc := LDL_Baseline] %>%
            .[Statin_Baseline == 1,
                LDLc := LDL_Baseline / cor.factor\$Factor] %>%
            .[, c("FID", "IID", "LDLc", "Centre", "Sex", "Age")] %>%
            na.omit %>%
            .[, ${pheno} := 0] %>%
            .[LDLc > 4.9, ${pheno} := 1]
    }
    fwrite(pheno, "${pheno}-binary.csv", na = "NA", quote = F)
    """
}

process process_phenotype_for_segregation{
    time '1h'
    memory '10G'
    queue 'express'
    module 'R/4.0.3'
    input:
        tuple   val(pheno),
                path(csv),
                path(cov),
                path(qc),
                path(dropout)
    output:
        tuple   val(pheno),
                path("${pheno}-adj.csv")
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    # Remove dropout, extract QCed samples and merge with covariate
    dropout <- fread("${dropout}", header = F)
    fam <- fread("${qc}", header = F)
    cov <- fread("${cov}", header = T)
    pheno <- fread("${csv}")
    pheno %>%
        .[!IID %in% dropout[, V1]] %>%
        .[IID %in% fam[, V2]] %>%
        merge(., cov, by = c("FID", "IID")) %>%
        .[, c("FID",
              "IID",
              "${pheno}",
              "Centre",
              "Batch",
              "Sex",
              "Age",
              paste("PC", 1:15, sep = "")), with = F] %>%
        .[, Batch := as.factor(Batch)] %>%
        .[, Centre := as.factor(Centre)] %>%
        na.omit %>%
        .[, ${pheno}Adj := paste("PC", 1:15, sep = "", collapse = "+") %>%
            paste("${pheno}~Sex+Age+Centre+Batch+", ., sep = "") %>%
            glm(., data = .SD, family = binomial) %>%
            resid] %>%
        fwrite(., "${pheno}-adj.csv")
    """
}

process extract_phenotype_from_sql{
    // This process is responsible for extracting the phenotypes
    // from the sqlite database using the phenotype specific sql file
    publishDir "result/phenotype", mode: 'symlink'
    label 'normal'
    input: 
        tuple   val(pheno),
                path(script),
                path(rscript),
                path(sql)
    output:
        tuple   val(pheno),
                path(rscript),
                path("${pheno}.csv")
    script:
    """
    sqlite3 ${sql} < ${script}
    """
}

process residualize_phenotypes{
    // This process will take in the phenotype specific Rscripts and run it to generate the 
    // required residualized phenotype
    label 'normal'
    input:
        tuple   val(pheno),
                path(rscript),
                path(phenoFile),
                path(fam),
                path(dropout),
                path(cov)
    output:
        tuple   val(pheno),
                path("${pheno}-adj")

    script:
    """
    Rscript ${rscript} ${phenoFile} ${fam} ${dropout} ${cov} ${pheno}-adj
    """
}

process extract_ibd_folds{
    module 'R/4.0.3'
    time '2h'
    memory '20G'
    queue 'express'
    input:
        tuple   val(pheno),
                path(phenoFile),
                path(cov),
                val(fold),
                val(type)
    output:
        tuple   val(pheno),
                val(fold),
                val(type),
                path("${pheno}-${fold}-${type}.training"),
                path("${pheno}-${fold}-${type}.validate")
    script:
    """
    #!/usr/bin/env Rscript
    library(data.table)
    library(magrittr)
    ibd <- fread("${phenoFile}")
    cov <- fread("${cov}")
    ibd[Fold == ${fold}] %>%
        fwrite( .,
                "${pheno}-${fold}-${type}.validate",
                sep = "\\t",
                na = "NA",
                quote =F
        )
    model <- paste("PC", 1:15, collapse = "+", sep = "") %>%
        paste0("IBD~Sex+Centre+Batch+", .) %>%
        as.formula
    if("${type}" == "Unsupervised"){
        ibd[Fold != ${fold} | is.na(Fold)] %>%
            .[, Centre := as.factor(Centre)] %>%
            .[, IBD := as.numeric(!is.na(subtype))] %>%
            merge(., cov, by=c("FID", "IID")) %>%
            .[, .(FID = FID,
                    IID = IID,
                    PhenoAdj = glm(model, data = .SD, family=binomial) %>%
                        resid)] %>%
            fwrite(.,
                "${pheno}-${fold}-${type}.training", 
                sep = "\\t", 
                na = "NA",
                quote =F) 
    }else{
        ibd[Fold != ${fold} | is.na(Fold)] %>%
            .[, Centre := as.factor(Centre)] %>%
            .[!is.na(subtype)] %>%
            .[, IBD := subtype] %>%
            merge(., cov, by=c("FID", "IID")) %>%
            .[, .(FID = FID,
                    IID = IID,
                    PhenoAdj = glm(model, data = .SD, family=binomial) %>%
                        resid)] %>%
            fwrite(.,
                "${pheno}-${fold}-${type}.training", 
                sep = "\\t", 
                na = "NA",
                quote =F) 
    }
    """
}
process generate_ibd_folds{
    module 'R/4.0.3'
    time '2h'
    memory '20G'
    queue 'express'
    input:
        tuple   val(pheno),
                path(rscript),
                path(phenoFile),
                path(fam),
                path(dropout),
                path(cov),
                val(fold)
    output:
        tuple   val(pheno),
                path("${pheno}-sub.csv"),
                path(cov)
    script:
    """
    #!/usr/bin/env Rscript
    library(data.table)
    library(magrittr)

    pheno <- fread("${phenoFile}")
    qc <- fread("${fam}")
    dropout <- fread("${dropout}", header = F)
    cov <- fread("${cov}")
    out <- "${pheno}-sub.csv"

    get_age <- function(x, y) {
        max(x, y, na.rm = T)
    }
    # Output samples used for subtyping
    ibd <- pheno %>%
        .[IID %in% qc[, V2]] %>%
        .[!IID %in% dropout[, V1]] %>%
        .[,IBD:= pmin(IBD, Crohns, Ulcerative, na.rm=T)]
    subset <- ibd %>%
        .[, hasCrohns := !is.na(Crohns)] %>%
        .[, hasUC := !is.na(Ulcerative)] %>%
        .[, subtype := NA] %>%
        .[hasCrohns == TRUE, subtype := 0] %>%
        .[hasUC == TRUE, subtype := 1] %>%
        .[, subtype := as.numeric(subtype)] %>%
        # Remove all control samples or comorbid cases
        .[!is.na(subtype) & !(hasCrohns & hasUC)] %>%
        .[, Age := pmax(Crohns, Ulcerative, na.rm=T)] %>%
        .[Age > 0] %>%
        .[, c("FID", "IID", "subtype", "Centre", "Sex")] %>%
        .[, Fold := sample(c(1:${fold}), .N, replace = T), by = subtype]
    ibd[is.na(Crohns) & is.na(Ulcerative) & is.na(IBD)] %>%
        .[, subtype := NA] %>%
        .[, Fold := NA] %>%
        .[, c("FID", "IID", "subtype", "Centre", "Fold", "Sex")] %>%
        rbind(., subset) %>%
        fwrite(.,
            out, 
            sep = "\\t",
            na = "NA",
            quote = F)
    """
}