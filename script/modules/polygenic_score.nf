process lassosum_analysis{
    label 'prsice'
    afterScript 'rm *bed'
    clusterOptions = '-P acc_psychgen -M 49152 -R select[mem>=2048] rusage[mem=2048]'
    input:
        tuple   val(tag),
                val(phenoName),
                path(pheno),
                path(snp),
                path(gwas),
                path(bed),
                path(bim),
                path(fam),
                path(lassosum),
                val(best),
                val(all)
    output:
        tuple   val(tag),
                path("${tag}-lassosum-best.gz"), emit: best optional true
        tuple   val(tag),
                path("${tag}-lassosum-all.gz"), emit: all optional true

    script:
    base=bed.baseName
    """
    # Reduce genotyep file size, which helps to speed up lassosum rather significantly
    plink \
        --bfile ${base} \
        --keep ${pheno} \
        --extract ${snp} \
        --make-bed \
        --out genotype
    
    all=""
    if [ "${all}" == "true" ]; then
        all="--all"
    fi
    score=""
    if [ "${best}" == "true" ]; then
        score="--score"
    fi
    Rscript ${lassosum} \
        --target genotype \
        --base ${gwas} \
        --snp SNP \
        --effective A1 \
        --reference A2 \
        --stat BETA \
        --pvalue P \
        --out ${tag}-lassosum \
        --thread ${task.cpus} \
        --pheno ${pheno} \
        --name PhenoAdj \
        --train ${pheno} \
        --Ncol N \
        \${score} \
        \${all}
    if [ "${best}" == "true" ]; then
        gzip ${tag}-lassosum-best 
    fi
    if [ "${all}" == "true" ]; then
        gzip ${tag}-lassosum-all
    fi
    """
}
process cross_trait_lassosum_cv_analysis{
    module 'R/4.0.3'
    module 'plink'
    cpus '22'
    time '12h'
    queue 'express'
    afterScript 'rm *bed'
    clusterOptions = '-P acc_psychgen -M 49152 -R select[mem>=2048] rusage[mem=2048]'
    input:
        tuple   val(traitA),
                val(traitB), 
                val(fold),
                val(type),
                val(analysis),
                path(training),
                path(validate),
                path(sumstat),
                path(bed),
                path(bim),
                path(fam),
                path(snp),
                val(xregion),
                path(lassosum)
    output:
        tuple   val(fold),
                val(type),
                val(analysis),
                val(traitA),
                val(traitB),
                val("lassosum"),
                path(training),
                path(validate),
                path("${traitA}-${traitB}-${fold}-${type}-lassosum-best.gz"),
                path("${traitA}-${traitB}-${fold}-${type}-lassosum-all.gz")

    script:
    base=bed.baseName
    """
    # We want to subset the plink file to speed up lassosum
    ls -lht /tmp | grep chois26 | awk '{print "rm -rf "\$NF}' | bash

    awk 'NR==FNR{print \$0} NR != FNR && FNR != 1 {print \$0}' ${training} ${validate} > keep
    # This does not account for multiple xregion
    chr=`sed 's/:/ /' <<< ${xregion} | awk '{print \$1}'`
    bp=`sed 's/:/ /' <<< ${xregion} | awk '{print \$2}' | awk -F "-" '{print \$1-1"\\t"\$2}'`
    echo -e "\${chr}\\t\${bp}\\tRemove" > exclude
    plink \
        --bfile ${base} \
        --keep keep \
        --extract ${snp} \
        --exclude range exclude \
        --make-bed \
        --out genotype
    check=`zcat ${sumstat} | head -n 1 | grep NMISS | wc -l`
    ncol="NMISS"
    if [ \$check -eq 0 ]; then
        ncol="N"
    fi
    Rscript ${lassosum} \
        --target genotype \
        --base ${sumstat} \
        --snp SNP \
        --effective A1 \
        --reference A2 \
        --stat Zscore \
        --pvalue P \
        --out ${traitA}-${traitB}-${fold}-${type}-lassosum \
        --thread ${task.cpus} \
        --pheno keep \
        --name PhenoAdj \
        --train ${training} \
        --Ncol \$ncol \
        --score \
        --all
    awk '{print \$1"\\t"\$2"\\tNA\\t"\$3}' ${traitA}-${traitB}-${fold}-${type}-lassosum-best | gzip > ${traitA}-${traitB}-${fold}-${type}-lassosum-best.gz
    cat ${traitA}-${traitB}-${fold}-${type}-lassosum-all | gzip > ${traitA}-${traitB}-${fold}-${type}-lassosum-all.gz
    """
}

process prsice_analysis{
    label 'prsice'
    afterScript "rm *.prsice"
    input:
        tuple   val(tag),
                val(phenoName),
                path(pheno),
                path(snp),
                path(gwas),
                path(bed),
                path(bim),
                path(fam),
                val(allScore)
    output:
        tuple   val(tag),
                path("${tag}.summary"), emit: summary optional true
        tuple   val(tag),
                path("${tag}.best.gz"), emit: best optional true
        tuple   val(tag),
                path("${tag}.all_score.gz"), emit: all optional true
    script:
    prefix = bed.baseName
    """
    all=""
    if [ "${allScore}" == "true" ]; then
        all="--no-regress --non-cumulate --inter 1e-3"
    fi
    PRSice \
        --base ${gwas} \
        --snp SNP  \
        --a1 A1 \
        --a2 A2 \
        --stat BETA \
        --pvalue P \
        --thread ${task.cpus} \
        --target ${prefix} \
        --keep ${pheno} \
        --pheno ${pheno} \
        --pheno-col ${phenoName} \
        --out ${tag} \
        --ultra \
        --beta \
        --extract ${snp} \
        \${all}
    if [ "${allScore}" == "true" ]; then
        gzip *all_score
    else
        gzip *best
    fi
    """
}
process cross_trait_prsice_cv_analysis{
    module 'cmake'
    cpus '22'
    time '12h'
    queue 'express'
    clusterOptions = '-P acc_psychgen -M 49152 -R select[mem>=2048] rusage[mem=2048]'
    input:
        tuple   val(traitA),
                val(traitB), 
                val(fold),
                val(type),
                val(analysis),
                path(training),
                path(validate),
                path(sumstat),
                path(bed),
                path(bim),
                path(fam),
                path(snp),
                val(xregion),
                path(prsice)
    output:
        tuple   val(fold),
                val(type),
                val(analysis),
                val(traitA),
                val(traitB),
                val("PRSice"),
                path(training),
                path(validate),
                path("${traitA}-${traitB}-${fold}-${type}.best.gz")
    script:
    name = bed.baseName
    """
    xrange=""
    if [ ! -z "${xregion}" ]; then
        xrange="--x-range ${xregion}"
    fi
    awk 'NR==FNR{print \$0} NR != FNR && FNR != 1 {print \$0}' ${training} ${validate} > keep
    ./${prsice} \
        --base ${sumstat} \
        --snp SNP  \
        --a1 A1 \
        --a2 A2 \
        --stat Zscore \
        --pvalue P \
        --thread ${task.cpus} \
        --target ${name} \
        --keep keep \
        --pheno ${training} \
        --pheno-col PhenoAdj \
        --out ${traitA}-${traitB}-${fold}-${type} \
        --ultra \
        --beta \
        --extract ${snp} \
        \${xrange}
    gzip *best
    """
}

process cross_trait_fast_prsice_cv_analysis{
    module 'cmake'
    cpus '22'
    time '12h'
    queue 'express'
    clusterOptions = '-P acc_psychgen -M 49152 -R select[mem>=2048] rusage[mem=2048]'
    input:
        tuple   val(traitA),
                val(traitB), 
                val(fold),
                val(type),
                val(analysis),
                path(training),
                path(validate),
                path(sumstat),
                path(bed),
                path(bim),
                path(fam),
                path(snp),
                val(xregion),
                path(prsice)
    output:
        tuple   val(fold),
                val(type),
                val(analysis),
                val(traitA),
                val(traitB),
                val("PRSice"),
                path("${traitA}-${traitB}-${fold}-${type}.all_score.gz")
    script:
    name = bed.baseName
    """
    # We use 1e-3 as step size to make sure we get high number of PRS (~500)
    xrange=""
    if [ ! -z "${xregion}" ]; then
        xrange="--x-range ${xregion}"
    fi
    awk 'NR==FNR{print \$0} NR != FNR && FNR != 1 {print \$0}' ${training} ${validate} > keep
    ./${prsice} \
        --base ${sumstat} \
        --snp SNP  \
        --a1 A1 \
        --a2 A2 \
        --stat Zscore \
        --pvalue P \
        --thread ${task.cpus} \
        --target ${name} \
        --keep keep \
        --pheno ${training} \
        --pheno-col PhenoAdj \
        --out ${traitA}-${traitB}-${fold}-${type} \
        --ultra \
        --no-regress \
        --non-cumulate \
        --inter 1e-3 \
        --beta \
        --extract ${snp} \
        \${xrange}
    gzip *all_score
    """
}

process prset_analysis{
    // This process is responsible for running the PRSet analysis. 
    // If keepBest is false, we will delete the best file, otherwise,
    // we will gzip the best file and provide it as an output
    label "prset"
    afterScript "rm *.prsice"
    input:
        tuple   val(tag),
                val(phenoName),
                path(pheno),
                path(snp),
                path(gwas),
                path(bed),
                path(bim),
                path(fam),
                path(gtf),
                val(wind5),
                val(wind3),
                val(perm),
                path(gmt),
                val(keepBest),
                val(highRes)
    output:
        tuple   val(tag),
                path("${tag}.summary"), emit: summary
        tuple   val(tag),
                path("${tag}.best.gz"), emit: best optional true
        tuple   val(tag),
                path("${tag}.snp"), emit: snp optional true
    stub:
    """
    touch ${tag}.summary
    if [[ "${keepBest}" != "false" ]]; then
        touch ${tag}.best
    fi
    """
    script:
    prefix=bed.baseName
    """
    wind5_com=""
    wind3_com=""
    if [ ! -z "${wind5}" ]; then
        wind5_com="--wind-5 "${wind5}kb
    fi
    if [ ! -z "${wind3}" ]; then
        wind3_com="--wind-3 "${wind3}kb
    fi
    # only do permutation if we are not doing high resolution prset
    highRes="--set-perm ${perm}"
    if [ "${highRes}" == "true" ]; then
        highRes="--upper 0.5"
    fi
    PRSice \
        --base ${gwas} \
        --target ${prefix} \
        --out ${tag} \
        --keep ${pheno} \
        --extract ${snp} \
        --pheno ${pheno} \
        --pheno-col ${phenoName} \
        --snp SNP \
        --pvalue P \
        --beta \
        --stat BETA \
        --a1 A1 \
        --a2 A2 \
        --gtf ${gtf} \
        --msigdb ${gmt} \
        --thread ${task.cpus} \
        --ultra \
        --print-snp \
        \${wind5_com} \
        \${wind3_com} \
        \${highRes}

    if [[ "${keepBest}" == "false" ]]; then
        rm ${tag}.best
    else
        gzip *best
    fi
    """
}

process modify_prset_result{
    label 'normal'
    input:
        tuple   val(pheno),
                path(summary)
    output:
        tuple   val(pheno),
                val("Set"),
                path("${pheno}-Set-prset.csv")
    script:
    """
    #!/usr/bin/env Rscript
    library(data.table)
    library(magrittr)
    fread("${summary}") %>%
        .[, Competitive.P := as.numeric(Competitive.P)] %>%
        .[!is.na(Competitive.P)] %>%
        .[, c("Set", "Coefficient", "Competitive.P", "P")] %>%
        setnames(., "P", "Self.P") %>%
        setnames(., "Competitive.P", "P") %>%
        .[, Software := "PRSet"] %>%
        .[, Type := "Set"] %>%
        .[, Trait := "${pheno}"] %>%
        fwrite(., "${pheno}-Set-prset.csv")
    """
}

process modify_simulated_prset_result{
    label 'normal'
    input:
        tuple   val(perm),
                val(numSet),
                val(herit),
                val(targetSize),
                val(baseSize),
                path(summary)
    output:
        tuple   val(perm),
                val(numSet),
                val(herit),
                val(targetSize),
                val(baseSize),
                path("${perm}-${numSet}-${herit}-${targetSize}-${baseSize}-prset.csv")
    script:
    """
    #!/usr/bin/env Rscript
    library(data.table)
    library(magrittr)
    fread("${summary}") %>%
        .[, Competitive.P := as.numeric(Competitive.P)] %>%
        .[!is.na(Competitive.P)] %>%
        .[, c("Set", "Coefficient", "Competitive.P", "P")] %>%
        setnames(., "P", "Self.P") %>%
        setnames(., "Competitive.P", "P") %>%
        .[, Software := "PRSet"] %>%
        .[, Perm := "${perm}"] %>%
        .[, NumSet := "${numSet}"] %>%
        .[, Heritability := "${herit}"] %>%
        .[, TargetSize := "${targetSize}"] %>%
        .[, BaseSize := "${baseSize}"] %>%
        fwrite(., "${perm}-${numSet}-${herit}-${targetSize}-${baseSize}-prset.csv")
    """
}


process first_pass_prset{
    module 'cmake'
    cpus '48'
    time '12h'
    queue 'express'
    clusterOptions = '-P acc_psychgen -M 31440 -R select[mem>=1024] rusage[mem=1024]'
    input:
        tuple   val(name),
                val(fold),
                path(training),
                path(validate),
                path(sumstat),
                path(bed),
                path(bim),
                path(fam),
                val(group),
                path(gtf),
                path(snp),
                val(wind3),
                val(wind5),
                val(xregion),
                path(prsice),
                path(gmt)
    output:
        tuple   val(group),
                val(name),
                val(fold),
                val("PRSet"),
                path(training),
                path(validate),
                path(sumstat),
                path("${name}-${fold}.summary")
    script:
    prefix = bed.baseName
    """
    ls -lht /tmp | grep chois26 | awk '{print "rm -rf "\$NF}' | bash
    wind5_com=""
    wind3_com=""
    if [ ! -z "${wind5}" ]; then
        wind5_com="--wind-5 "${wind5}kb
    fi
    if [ ! -z "${wind3}" ]; then
        wind3_com="--wind-3 "${wind3}kb
    fi
    xrange=""
    if [ ! -z "${xregion}" ]; then
        xrange="--x-range ${xregion}"
    fi
    cat ${gmt} | grep -v _NULL > gmt
    cat ${training} ${validate} > keep
    
    ./${prsice} \
        --base ${sumstat} \
        --snp SNP  \
        --a1 A1 \
        --a2 A2 \
        --stat Zscore \
        --pvalue P \
        --gtf ${gtf} \
        --msigdb gmt \
        --thread ${task.cpus} \
        --target ${prefix} \
        --keep keep \
        --pheno ${training} \
        --pheno-col PhenoAdj \
        --out ${name}-${fold} \
        --extract ${snp} \
        --ultra \
        --beta \
        \${wind3_com} \
        \${wind5_com} \
        \${xrange} \
        --set-perm 10000
    """
}


process ibd_prset_analysis{
    module 'cmake'
    cpus '48'
    time '24h'
    queue 'premium'
    clusterOptions = '-P acc_psychgen -M 63219 -R select[mem>=2048] rusage[mem=2048]'
    afterScript "rm *.best *.prsice"
    input:
        tuple   val(name),
                val(fold),
                val(type),
                path(training),
                path(validate),
                val(gwasPheno),
                path(snp),
                path(gwas),
                path(bed),
                path(bim),
                path(fam),
                path(gtf),
                val(wind3),
                val(wind5),
                path(prsice),
                path(gmt)
    output:
        tuple   val(name),
                val(gwasPheno),
                val(fold),
                val(type),
                path("${name}.summary"),
                path(training),
                path(validate),
                path(gwas),
                path(snp),
                path(bed),
                path(bim),
                path(fam),
                path(gtf),
                val(wind3),
                val(wind5),
                path(prsice),
                path(gmt)
    script:
    prefix=bed.baseName
    """
    wind5_com=""
    wind3_com=""
    if [ ! -z "${wind5}" ]; then
        wind5_com="--wind-5 "${wind5}kb
    fi
    if [ ! -z "${wind3}" ]; then
        wind3_com="--wind-3 "${wind3}kb
    fi
    cat ${training} ${validate} >> keep

    ./${prsice} \
        --base ${gwas} \
        --target ${prefix} \
        --out ${name} \
        --set-perm 10000 \
        --keep keep \
        --extract ${snp} \
        --pheno ${training} \
        --pheno-col PhenoAdj \
        --snp SNP \
        --pvalue P \
        --beta \
        --stat BETA \
        --a1 A1 \
        --a2 A2 \
        --gtf ${gtf} \
        --msigdb ${gmt} \
        --thread ${task.cpus} \
        --ultra \
        \${wind5_com} \
        \${wind3_com}
    """
}

process ibd_high_res_analysis{
    module 'cmake'
    cpus '48'
    time '24h'
    queue 'premium'
    clusterOptions = '-P acc_psychgen -M 63219 -R select[mem>=2048] rusage[mem=2048]'
    input:
        tuple   val(name),
                val(gwasPheno),
                val(fold),
                val(type),
                path(summary),
                path(training),
                path(validate),
                path(gwas),
                path(snp),
                path(bed),
                path(bim),
                path(fam),
                path(gtf),
                val(wind3),
                val(wind5),
                path(prsice),
                path(gmt)
    output:
        tuple   val(name),
                val(gwasPheno),
                val(fold),
                val(type),
                path(training),
                path(validate),
                path("${name}-high-res.best.gz")
    script:
    prefix=bed.baseName
    """
    wind5_com=""
    wind3_com=""
    if [ ! -z "${wind5}" ]; then
        wind5_com="--wind-5 "${wind5}kb
    fi
    if [ ! -z "${wind3}" ]; then
        wind3_com="--wind-3 "${wind3}kb
    fi
    cat ${training} ${validate} >> keep
    awk 'NR == FNR && FNR > 2 && \$12< 0.05 { a[\$2]=1} NR != FNR && \$1 in a {print}' ${summary} ${gmt} > trim_gmt
    ./${prsice} \
        --base ${gwas} \
        --target ${prefix} \
        --out ${name}-high-res \
        --keep keep \
        --extract ${snp} \
        --pheno ${training} \
        --pheno-col PhenoAdj \
        --snp SNP \
        --pvalue P \
        --beta \
        --stat BETA \
        --a1 A1 \
        --a2 A2 \
        --gtf ${gtf} \
        --msigdb trim_gmt \
        --thread ${task.cpus} \
        --ultra \
        \${wind5_com} \
        \${wind3_com}
    gzip *best
    """
}


process ibd_prsice_analysis{
    module 'cmake'
    cpus '48'
    time '24h'
    queue 'premium'
    clusterOptions = '-P acc_psychgen -M 63219 -R select[mem>=2048] rusage[mem=2048]'
    afterScript "rm *.prsice"
    input:
        tuple   val(name),
                val(fold),
                val(type),
                path(training),
                path(validate),
                val(gwasPheno),
                path(snp),
                path(gwas),
                path(bed),
                path(bim),
                path(fam),
                path(gtf),
                val(wind3),
                val(wind5),
                path(prsice),
                path(gmt)
    output:
        tuple   val(name),
                val(gwasPheno),
                val(fold),
                val(type),
                path("${name}.summary"),
                path("${name}.best.gz"),
                path(training),
                path(validate)
    script:
    prefix=bed.baseName
    """
    cat ${training} ${validate} >> keep

    ./${prsice} \
        --base ${gwas} \
        --target ${prefix} \
        --out ${name} \
        --keep keep \
        --extract ${snp} \
        --pheno ${training} \
        --pheno-col PhenoAdj \
        --snp SNP \
        --pvalue P \
        --beta \
        --stat BETA \
        --a1 A1 \
        --a2 A2 \
        --thread ${task.cpus} \
        --ultra
        gzip *best
    """
}

process extract_significant_gmt{
    label 'tiny'
    input:
        tuple   val(tag),
                path(summary),
                path(gmt)
    output:
        tuple   val(tag),
                path("trim_gmt")
    script:
    """
    awk 'NR == FNR && FNR > 2 && \$12< 0.05 { a[\$2]=1} NR != FNR && \$1 in a {print}' ${summary} ${gmt} > trim_gmt
    """
}