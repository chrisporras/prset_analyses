process select_causal_variants{
    label 'more_mem'
    input:
        tuple   path(annot),
                val(perm),
                val(seed),
                val(numSet)
    output:
        tuple   val(perm),
                val(seed),
                val(numSet),
                path("${perm}-${numSet}.snp"), emit: snps
        path "${perm}-${numSet}.count", emit: count
        tuple   val(perm),
                path ("${perm}-${numSet}.rank"), emit: rank
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    set.seed(${seed})
    annot <- fread("${annot}") %>%
        .[,-c("CHR", "BP", "CM", "Base")] 
    total <- nrow(annot)
    # Remove background set and sets with 0 SNPs
    annot %<>% 
        .[,-c("Background")] %>%
        .[rowSums(annot[,-1])!=0]
    # Random select gene sets, 1 is SNP ID
    select <- sample(2:ncol(annot), size=${numSet}) %>%
        sort %>%
        c(1, .)
    sets <- annot[, ..select]
    # randomly generate portion of causal SNPs for each selected pathways
    portion <- rep(seq(5,50,5)/100, each=${numSet}/10) %>%
        sample

    snps <- NULL
    for(i in 1:length(portion)){
        name <- colnames(sets)[i+1]
        s <- sets[get(name) != 0, "SNP"] %>%
            unlist %>%
            sample(., size=length(.)*portion[i]) 
        snps %<>% 
            rbind(., data.table(SNP=s)) %>%
            unique
    }
    fwrite(snps, "${perm}-${numSet}.snp")
    # Now calculate the ranking of all Sets
    results <- NULL
    cols <- colnames(annot)[-1]
    for(i in cols){
        s <- annot[get(i)!=0,"SNP"] 
        results %<>% rbind(., 
                data.table( Name=i, 
                            Total=nrow(s), 
                            Index=sum(s[,SNP]%in% snps[,SNP])
                            ))
    }
    results %>%
        .[, Perm := ${perm}] %>%
        .[, NSet := ${numSet}] %>%
        fwrite(., "${perm}-${numSet}.rank")
    data.table(Ratio.Causal=nrow(unique(snps))/total, Perm=${perm}, NSet=${numSet}) %>%
        fwrite(., "${perm}-${numSet}.count")
    """
}

process calculate_xbeta{
    label 'normal'
    input:
        tuple   val(perm),
                val(seed),
                val(numSet),
                path(snps),
                path(bed),
                path(bim),
                path(fam)
    output:
        tuple   val(perm),
                val(seed),
                val(numSet),
                path("${perm}.xbeta")
    script:
    base=bed.baseName
    """
    BBS \
        --input ${base} \
        --effect 2 \
        --extract ${snps} \
        --out ${perm} \
        --seed ${seed} \
        --herit 0.8 \
        --nsnp 500000 \
        --std
    """        
}

process simulate_phenotype{
    label 'normal'
    input:
        tuple   val(perm),
                val(seed),
                val(numSet),
                path(xbeta),
                val(herit)
    output:
        tuple   val(perm),
                val(seed),
                val(numSet),
                val(herit),
                path("${perm}.pheno")
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    set.seed(${seed})
    xbeta <- fread("${xbeta}") %>%
        setnames(., colnames(.)[3], "xb")
    set.herit <- ${herit}
    sd.xb <- sd(xbeta[,xb])
    phenotype <- xbeta[,xb:=xb/sd.xb*sqrt(set.herit)] %>%
        .[, Pheno := xb + rnorm(.N, sd = sqrt(1-${herit}))] %>% 
        .[, c("FID", "IID", "Pheno")] %>%
        .[,Sample:=sample(c(0,1), replace=T, size=.N, prob=c(2/3,1/3))]
    fwrite(phenotype, "${perm}.pheno")
    """

}


process select_samples{
    label 'normal'
    input:
        tuple   val(perm),
                val(seed),
                val(numSet),
                val(herit),
                path(pheno),
                val(type),
                val(size)
    output:
        tuple   val(perm),
                val(numSet),
                val(herit),
                val(size),
                val(type),
                path("${perm}-${size}.${type}")
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    set.seed(${seed})
    phenotype <- fread("${pheno}")
    target.size <- ${size}
    sample <- 0
    if("${type}" == "target"){
        sample <- 1
    }
    phenotype[Sample==sample] %>%
        .[,-c("Sample")] %>%
        .[sample(1:.N, size=target.size)] %>%
        fwrite(., "${perm}-${size}.${type}", sep="\\t")
    """
}

process merge_rank{
    module 'R/4.0.3'
    cpus '1'
    memory '20G'
    time '1h'
    input:
        path(ranks)
    output:
        path("Ranking")
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    files <- strsplit("${ranks}", split=" ") %>%
        unlist
    res <- NULL
    for(i in files){
        res %<>% rbind(., fread(i))
    }
    fwrite(res, "Ranking")
    """
}