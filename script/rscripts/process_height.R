library(data.table)
library(magrittr)
args <- commandArgs(trailingOnly = TRUE)
pheno <- fread(args[1])
qc <- fread(args[2])
dropout <- fread(args[3], header = F)
cov <- fread(args[4])
out <- args[5]

inverse.normal <- function(x) {
    qnorm((rank(x, na.last = "keep") - 0.5) / sum(!is.na(x)))
}
height.model <-
    paste0("Height~Age+Age2+Centre+Batch+",
           paste("PC", 1:15, collapse = "+", sep = "")) %>%
    as.formula

pheno[, Age2 := Age ^ 2] %>%
    .[IID %in% qc[, V2]] %>%
    .[!IID %in% dropout[, V1]] %>%
    merge(., cov, by = c("FID", "IID")) %>%
    .[, c("FID",
          "IID",
          "Age",
          "Age2",
          "Sex",
          "Centre",
          "Batch",
          "Height",
          paste("PC", 1:15, sep = "")), with = F] %>%
    .[, Centre := as.factor(Centre)] %>%
    .[, Batch := as.factor(Batch)] %>%
    na.omit %>%
    .[, .(
        FID = FID,
        IID = IID,
        Pheno = lm(height.model, data = .SD) %>%
            resid),
        by = Sex] %>%
    .[, Pheno := inverse.normal(Pheno)] %>%
    setnames(., "Pheno", "Height") %>%
    .[, c("FID", "IID", "Height", "Sex")] %>%
    fwrite(.,
           out,
           sep = "\t",
           na = "NA",
           quote = F)