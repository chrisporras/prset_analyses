#!/usr/bin/env nextflow

////////////////////////////////////////////////////////////////////
//
//      This script is responsible to perform the real data analyses, 
//  which includes the MalaCards Biochemical pathway analysis and
//  the Cell type / Tissue specificity analysis. 
//
//      This script requires the GMT files, the gene location files, 
//  and the LD scores generated from the prepare_workspace script.
//  It also required an external summary statistic file and the 
//  csv file containing experts' ranking of disease relevance
//
//      On our server, if given 48 threads, this script should take 
//  around 7 hours to complete for most traits
//
////////////////////////////////////////////////////////////////////

// TODO: Do we want to do a genic vs non-genic PRSice? To illustrate most signals are within 
// the genic region?

nextflow.enable.dsl=2
params.version=false
params.help=false
version='0.0.2'
timestamp='2021-02-24'
if(params.version) {
    System.out.println("")
    System.out.println("Run Set Analysis - Version: $version ($timestamp)")
    exit 1
}


if(params.help){
    System.out.println("")
    System.out.println("Run Set Analysis - Version: $version ($timestamp)")
    System.out.println("Usage: ")
    System.out.println("    nextflow run 2_real_data_analysis.nf [options]")
    System.out.println("Mandatory arguments:")
    System.out.println("    --loc         Gene location file for MAGMA")
    System.out.println("    --gtf         GTF file for PRSet")
    System.out.println("    --gmt         Directory containing the GMT files")
    System.out.println("    --bfile       Genotype file prefix ")
    System.out.println("    --fam         QCed fam file ")
    System.out.println("    --snp         QCed SNP file ")
    System.out.println("    --json        File containing all phenotype related information")
    System.out.println("    --cov         Covariate file")
    System.out.println("    --dropout     dropout samples")
    System.out.println("    --sql         UK biobank SQL data base")
    System.out.println("    --gtex        Cell type specificity file  ")
    System.out.println("    --scores      Folder contains all precalculated ld scores")
    System.out.println("    --freq        Minor allele frequency information required by LDSC")
    System.out.println("    --malacard    Malacard score for gene sets")
    System.out.println("    --gene        Malacard score of individual gene")
    System.out.println("    --expert      CSV containing expert opion")
    System.out.println("Options:")
    System.out.println("    --wind3       Padding to 3' end ")
    System.out.println("    --wind5       Padding to 5' end")
    System.out.println("    --perm        Number of permutation for PRSet")
    System.out.println("    --thread      Number of thread use")
    System.out.println("    --help        Display this help messages")
} 


////////////////////////////////////////////////////////////////////
//                  Helper Functions
////////////////////////////////////////////////////////////////////
def fileExists(fn){
   if (fn.exists()){
       return fn;
   }else
       error("\n\n-----------------\nFile $fn does not exist\n\n---\n")
}

def gen_file(a, bgen){
    return bgen.replaceAll("@",a.toString())
}

def get_chr(a, input){
    if(input.contains("@")){
        return a
    }
    else {
        return 0
    }
}


// Trying to parse the JSON input
import groovy.json.JsonSlurper
def jsonSlurper = new JsonSlurper()
// new File object from your JSON file
def ConfigFile = new File("${params.json}")
// load the text from the JSON
String ConfigJSON = ConfigFile.text
// create a dictionary object from the JSON text
def phenoConfig = jsonSlurper.parseText(ConfigJSON)
////////////////////////////////////////////////////////////////////
//                  Module inclusion
////////////////////////////////////////////////////////////////////
include {   standardize_summary_statistic
            filter_summary_statistic
            filter_bed
            perform_gwas
            meta_analysis
            modify_metal    }   from './modules/basic_genetic_analyses'
include {   malacards_score_analysis
            prep_null_for_perm
            perform_perm_kendall
            combine_kendall_null
            generate_ensembl_map    }   from './modules/generate_biochemical_sets'
include {   specificity_analysis  }   from './modules/generate_specificity_sets'
include {   extract_phenotype_from_sql
            residualize_phenotypes  }   from './modules/phenotype_extraction'
include {   prset_analysis
            modify_prset_result    }   from './modules/polygenic_score'
include {   combine_gmt }   from './modules/misc'
include {   get_overlap_snps
            perform_annotation
            magma_genotype
            magma_sumstat
            magma_meta
            magma_gene_set
            magma_specificity
            modify_magma_output}   from './modules/magma'
include {   munge_sumstats
            ldsc_biochemical_analysis
            ldsc_specificity_analysis
            modify_ldsc_result
            clean_freq  }   from './modules/calculate_ldscore'
////////////////////////////////////////////////////////////////////
//                  Setup Channels
////////////////////////////////////////////////////////////////////
biochemGMT = Channel.fromPath("${params.gmt}/*.gmt") 
specMAGMA=Channel.fromPath("${params.gtex}/*magma")
specGMT=Channel.fromPath("${params.gtex}/*gmt") 

gtf = Channel.fromPath("${params.gtf}")
genotype = Channel.fromFilePairs("${params.bfile}.{bed,bim,fam}", size:3 , flat: true){ file -> file.baseName } \
    | ifEmpty{ error "No matching plink files "} \
    | map{ a -> [ fileExists(a[1]), fileExists(a[2]), fileExists(a[3])]}
sql = Channel.fromPath("${params.sql}")
dropout = Channel.fromPath("${params.dropout}")
qcFam = Channel.fromPath("${params.fam}")
snp = Channel.fromPath("${params.snp}")
wind3 = Channel.of("${params.wind3}")
wind5 = Channel.of("${params.wind5}")
cov = Channel.fromPath("${params.cov}")

pheno = Channel.from(phenoConfig.collect{ content ->
    [   content.name, 
        file(content.phenoFile), 
        file(content.rscript)
    ]})
geneLoc=Channel.fromPath("${params.loc}")
chromosome = Channel.value(1..22).flatten()
freq = chromosome \
    | map{ a -> [   get_chr(a, "${params.freq}"), 
                    fileExists(file(gen_file(a, "${params.freq}")))]}
                    

sumstat = Channel.from(phenoConfig.collect{ content ->
    [   content.name, 
        file(content.gwas), 
        content.rsid,
        content.a1,
        content.a2,
        content.statistic,
        content.pvalue,
        content.se,
        content.ncol,
        content.sampleSize,
        content.isBeta
    ]})
xregions = Channel.from(phenoConfig.collect{ content ->
    [   content.name,
        content.xregion
    ]})
malacard = Channel.fromPath("${params.malacard}")
gene = Channel.fromPath("${params.gene}")
expert = Channel.fromPath("${params.expert}")
score = Channel.fromPath("${params.scores}/*")

workflow{
    combine_gmt(biochemGMT.collect(), specGMT.collect())
    phenotype_preprocessing()
    genotype_preprocessing(
        phenotype_preprocessing.out
    )
    prset_analyses(
        phenotype_preprocessing.out,
        genotype_preprocessing.out.sumstat,
        genotype_preprocessing.out.snp,
        genotype_preprocessing.out.geno,
        combine_gmt.out)
    magma_analyses(
        phenotype_preprocessing.out,
        genotype_preprocessing.out.sumstat,
        genotype_preprocessing.out.snp,
        genotype_preprocessing.out.geno,
        combine_gmt.out)


    ldsc_analyses(
        phenotype_preprocessing.out,
        genotype_preprocessing.out.sumstat,
        genotype_preprocessing.out.snp,
        genotype_preprocessing.out.geno,
        combine_gmt.out)

    downstream_analyses(
        prset_analyses.out,
        magma_analyses.out,
        ldsc_analyses.out,
        combine_gmt.out
    )
}

workflow phenotype_preprocessing{
    // 1. Extract Phenotype from SQL 
    pheno \
        | filter{ a -> a[0] != "SCZ"} \
        | combine(sql) \
        | extract_phenotype_from_sql 
    // 1. Or in the case of PGC SCZ, the phenotype file is directly provided
    //    Noted: Because we SCZ have a completely different set of input, we
    //    have to run it separately (and have a mock dropout file)
    pheno \
        | filter{ a -> a[0] == "SCZ"} \
        | map{  a -> [  a[0],   // phenotype name
                        a[2],   // Rscript
                        a[1]]   /* Phenotype file */} \
        | mix(extract_phenotype_from_sql.out ) \
        | combine(qcFam) \
        | combine(dropout) \
        | combine(cov) \
        | residualize_phenotypes
    emit:
        residualize_phenotypes.out
    // Main reason why we ask the script to extract and residualize the phenotype
    // is such that we maximize the number of steps that are performed within the
    // pipeline, thus reduce manual processing of data (e.g. previous scheme where
    // the residualized phenotype is manually extracted before running the pipeline)
}

workflow genotype_preprocessing{
    take: pheno
    main:
        // 1. We prefilter the genotype file to ensure we are 
        //    using the exact same input for all software
        //    Noted that we included the phenotype here so that 
        //    we can reduce the size of the result genotype file
        //    by removing samples without the phenotype information
        sumstat \
            | standardize_summary_statistic \
            | combine(snp) \
            | combine(genotype) \
            | combine(xregions, by: 0) \
            | combine(Channel.of("T")) /*Want to filter out ambiguous SNPs*/\
            | filter_summary_statistic 
        filter_summary_statistic.out.snp \
            | combine(pheno, by: 0) \
            | combine(genotype)\
            | filter_bed
    emit:
        sumstat=filter_summary_statistic.out.sumstat
        snp = filter_summary_statistic.out.snp
        // bed file filtering is really un-necessary for PRSet, but to 
        // ensure consistency between method, we will do prefiltering here
        geno=filter_bed.out
}

workflow prset_analyses{
    take: pheno
    take: gwas
    take: overlapSNP
    take: geno
    take: gmt
    main:
        // 1. Run PRSet with 10,000 set based permutation
        keep_best = Channel.of("false")
        high_resolution = Channel.of("false")
        pheno \
            | map{ a -> [   a[0],   // phenotype
                            a[0],   // phenotype name
                            a[1]]}  /* phenotype file*/\
            | combine(overlapSNP, by: 0) \
            | combine(gwas, by: 0) \
            | combine(geno, by: 0) \
            | combine(gtf) \
            | combine(wind5) \
            | combine(wind3) \
            | combine(Channel.of("${params.perm}")) \
            | combine(gmt) \
            | combine(keep_best) /* don't need best file*/\
            | combine(high_resolution) /* don't do high resolution */\
            | prset_analysis
        prset_analysis.out.summary \
            | modify_prset_result
    emit:
        modify_prset_result.out
}

workflow magma_analyses{
    take: pheno
    take: gwas
    take: overlapSNP
    take: geno
    take: gmt
    main:
        // 1. Generate the required annotation file for MAGMA
        overlapSNP \
            | combine(geno, by: 0) \
            | get_overlap_snps \
            | combine(geneLoc) \
            | combine(wind5) \
            | combine(wind3) \
            | perform_annotation
        // 2. Run MAGMA on the target genotype in order to obtain the required
        //    gene based statistic
        pheno \
            | map{ a -> [   a[0],   // phenotype
                            a[0],   // phenotype name
                            a[1]]}  /* phenotype file*/\
            | combine(perform_annotation.out, by: 0) \
            | combine(geno, by: 0) \
            | magma_genotype
        // 3. Perform MAGMA analysis on the base summary statistic (genotype is used for 
        //    LD calculation)
        gwas \
            | combine(perform_annotation.out, by: 0) \
            | combine(geno, by: 0) \
            | magma_sumstat
        // 4. Meta analyzed the base and target results
        magma_sumstat.out \
            | combine(magma_genotype.out, by: 0) \
            | magma_meta
        // 5. Perform set based MAGMA analysis on the biochemical pathways
        //    and the specificity pathways
        magma_meta.out \
            | combine(gmt) \
            | magma_gene_set
        // 6. Also perform the cell type specificity analysis developed by MAGMA
        //    as a sanity check
        specMAGMA \
            | combine(magma_meta.out) \
            | magma_specificity

        // 7. Harmonize MAGMA output
        magma_specificity.out \
            | groupTuple(by: [0,1]) \
            | mix(magma_gene_set.out) \
            | modify_magma_output 
            
    emit:
        modify_magma_output.out
}

workflow ldsc_analyses{
    take: pheno
    take: gwas
    take: overlapSNP
    take: geno
    take: gmt
    main:
    // 1. Perform GWAS on target data
    // 2. Meta-analyze summary statistics of base with target
        pheno \
            | map{ a -> [   a[0],   // phenotype
                            a[0],   // phenotype name
                            a[1]]}  /* phenotype file*/\
            | combine(overlapSNP, by: 0) \
            | combine(geno, by: 0) \
            | perform_gwas \
            | standardize_summary_statistic \
            | combine(gwas, by: 0) \
            | meta_analysis \
            | modify_metal \
            | munge_sumstats
    // 3. Extract LD score for the Genic region (Control)
    //    To avoid complication, we assume all our inputs were generated
    //    using the preparation script, thus follow a known format, which 
    //    allow us to extract the required data accordingly
    //    The filter functions here are just filtering out the corresponding 
    //    LDSC files
    control = score \
        | filter{ a -> (a =~ /Control/) }
    specificity = score \
        | filter{ a -> (a =~ /:/) } \
        | filter { a -> !(a =~ /GO:/ || a =~ /MP:/)}
    biochemical = score \
        | filter { a -> !(a =~ /Control/)} \
        | filter { a -> (a =~ /GO:/ || a =~ /MP:/ || !(a =~ /:/)) } \
        | filter { a -> !(a =~ /baseline/ || a =~ /weight/)}
    baseline = score \
        | filter { a -> (a =~ /baseline/) }
    weight = score \
        | filter { a -> (a =~ /weight/) }
    // 4. Clean up Freq file so that it only contain SNPs in the annotation
    frq = freq \
        | combine(baseline) \
        | clean_freq \
        | groupTuple(by: 0)
    // 5. Run specificity analysis
    
    munge_sumstats.out \
        | combine(specificity) \
        | combine(control) \
        | combine(baseline) \
        | combine(weight) \
        | ldsc_specificity_analysis
    // 6. Run the biochemical analysis
    munge_sumstats.out \
        | combine(biochemical) \
        | combine(baseline) \
        | combine(weight) \
        | combine(frq) \
        | ldsc_biochemical_analysis
    
    // 7. Harmonize LDSC output
    ldsc_specificity_analysis.out \
        | mix(ldsc_biochemical_analysis.out) \
        | groupTuple(by: [0,1]) \
        | modify_ldsc_result
    emit:
        modify_ldsc_result.out
}


workflow downstream_analyses{
    take: prsetRes
    take: magmaRes
    take: ldscRes
    take: gmt
    main:
        prsetRes \
            | combine(magmaRes, by: [0, 1]) \
            | combine(ldscRes, by: [0, 1]) \
            | combine(malacard) \
            | malacards_score_analysis 

        generate_ensembl_map(gtf)
        prsetRes \
            | combine(magmaRes, by: [0, 1]) \
            | combine(ldscRes, by: [0, 1]) \
            | combine(generate_ensembl_map.out.ensembl) \
            | combine(gmt) \
            | combine(gene) \
            | combine(Channel.of(10000)) \
            | prep_null_for_perm \
            | combine(Channel.of(100)) /* Size of each permutation*/\
            | combine(Channel.of(1..100)) /* Number of permutation group to process*/\
            | perform_perm_kendall \
            | groupTuple \
            | combine_kendall_null
        // there are two type of LDSC and MAGMA cell type specificity output, 
        // we will still try to handle them here, but will only include them
        // in supplementary as that is a lot
        spec = magmaRes \
            | combine(ldscRes, by: [0,1]) \
            | filter{ a -> a[1] == "Specificity"} \
            | map{  a -> [  a[0],
                            a[2],
                            a[3]]} 
        prsetRes\
            | combine(magmaRes, by: [0, 1]) \
            | combine(ldscRes, by: [0, 1]) \
            | combine(spec, by: 0) \
            | combine(expert) \
            | specificity_analysis
}